import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import * as pdf from 'html-pdf';
import * as fs from 'fs';
import { renderModuleFactory } from '@angular/platform-server';
import { ExistingProvider, StaticProvider, ValueProvider } from '@angular/core';
import { enableProdMode } from '@angular/core';
import * as express from 'express';
import { join } from 'path';
import * as bodyParser from 'body-parser';
import { PDFProvider } from './pdf-provider';
import { IPDFData } from '../src/app/interfaces/pdf-data';
import * as request from 'request';
import { PDFData } from './pdf-data';
import * as ip from 'ip';

let config = require('../src/assets/config.json');

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();
app.use(bodyParser.json({ limit: '500mb' }));
// app.use(bodyParser.urlencoded({ extended: true, limit: '500mb' }));
const PORT = 5555;
const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { ServerAppModuleNgFactory, LAZY_MODULE_MAP } = require('../dist/server/main.bundle');

// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';

// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';

app.engine('html', ngExpressEngine({
  bootstrap: ServerAppModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));
app.set('views', __dirname);

const options = {
  format: 'A4',
  phantomPath: './node_modules/phantomjs-prebuilt/bin/phantomjs',
  script: './node_modules/html-pdf/lib/scripts/pdf_a4_portrait.js',
  border: {
    top: '.25in',
    right: '.25in',
    bottom: '.25in',
    left: '.25in'
  },
  header: {
    height: '18mm',
  },
  footer: {
    height: '18mm',
    contents: {
    }
  },
};

const index = require('fs')
  .readFileSync(join(DIST_FOLDER, 'browser', 'index.html'), 'utf8')
  .toString();

config = JSON.parse(fs.readFileSync(join(DIST_FOLDER, 'server', 'assets', 'config.json'), 'utf8'));


app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/test', (req, res) => {
  request(PDFData.test(PORT), function (err, response, body) {
    if (err) {
      res.status(500).send(err);
      return console.log(err);
    }
    res.status(200).send(`Test successfully.  File is available here: ${config.location}temp.pdf`);
  });
});

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));

app.post('/pdf', (req, res) => {
  const pdfData = req.body;
  let csia = `
  <span class="pl1" *ngIf="pdfData.user.CSIA">
    <span class="bold">CSIA:</span>
    ${pdfData.user.CSIA}
  </span>`;
  if (pdfData.user.CSIA === undefined || !pdfData.user.CSIA.length) {
    csia = '';
  }
  options.footer.contents = {
    default: `
    <div class="pt1 mt1 border-top flex items-center" style="border-color: #cecece; font-family: 'Muli', sans-serif;font-size: 8px;">
      <div class="mt1 flex-auto">
        <span class="bold">Prepared By:</span>
        ${pdfData.user.firstName} ${pdfData.user.lastName}
        ${csia}
      </div>
      <div>page: {{page}}</div>
    </div>
  ` };
  renderModuleFactory(ServerAppModuleNgFactory, {
    url: req.path,
    document: index,
    extraProviders: [
      provideModuleMap(LAZY_MODULE_MAP),
      PDFProvider.getProvider(pdfData)
    ]
  }).then(html => {
    pdf.create(html, options).toFile(`${config.location}temp.pdf`, function (err, resp) {
      if (err) {
        res.status(500).send(err);
        return console.log(err);
      }
      res.status(200).download(resp.filename);
    });
    // res.status(200).send(html);
  });
});

// Start up the Node server
app.listen(PORT, () => {
  const address = ip.address();
  console.log(`Node server listening on http://${address}:${PORT}`);
});
