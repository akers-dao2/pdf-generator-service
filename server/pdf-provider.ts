import { InjectionToken, ValueProvider } from '@angular/core';

class PDFProvider {
    static getProvider(useValue) {
        return { provide: 'PDFData', useValue } as ValueProvider;
    }
}

export { PDFProvider };
