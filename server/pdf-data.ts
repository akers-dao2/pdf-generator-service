// tslint:disable:max-line-length

export class PDFData {
    public static test(port: number) {
        return {
            method: 'POST',
            url: `http://localhost:${port}/pdf`,
            headers:
            {
                'postman-token': '38355dd0-f51f-d570-5d2a-78a8de78ace2',
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                authorization: 'Basic YXBpOlJ6M3hXenlULTVHc3g4d21pX2lraVhKR0lSaVp3WEY0'
            },
            body:
            {
                client:
                {
                    address: '1234 Everywhere',
                    city: 'nowhere',
                    firstName: 'One',
                    lastName: 'More',
                    state: 'PA',
                    township: 'Whitehall\r',
                    zip: '22553',
                    key: '-L4GxinXVMpizQbg3Opy'
                },
                user:
                {
                    CSIA: '',
                    adminstration: true,
                    createJob: true,
                    crew: '-Kji8RBAwdvboXfwiE7D',
                    deleteJob: true,
                    editJob: true,
                    emailAddress: 'donnie.cloen@mac.com',
                    firstName: 'Donnie',
                    jobListView: 'today',
                    key: '-KglUuWhQ3t1MoDCqMaq',
                    lastName: 'Cloen',
                    password: 'password',
                    printJob: true
                },
                commentsAndRecommendations:
                    [{
                        applianceType: 0,
                        cap: { status: 1 },
                        condition:
                        {
                            comments: [3],
                            pictures:
                                [{
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=13535d5f-c00f-4831-b87f-1f52adb244c9',
                                    fileName: 'condition_0001_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'Condition',
                                    uuid: '0001'
                                },
                                {
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_1_-L4LsyR78Itb1XaqwpY6?alt=media&token=d29d3eca-827b-4a65-a57d-751283949d89',
                                    fileName: 'condition_0001_1_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 1,
                                    name: 'Condition',
                                    uuid: '0001'
                                },
                                {
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_2_-L4LsyR78Itb1XaqwpY6?alt=media&token=246b955c-9ff4-485a-8232-23f2ed7d4e88',
                                    fileName: 'condition_0001_2_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 2,
                                    name: 'Condition',
                                    uuid: '0001'
                                }],
                            recommendations: [0],
                            status: 0
                        },
                        id: 72331,
                        inside1: '7',
                        inside2: '0',
                        inside3: '7',
                        inside4: '0',
                        insulation: { status: 2 },
                        length: '32',
                        material: 'Terra Cotta',
                        name: 'Chimney-1: Flue/Vent-1',
                        outside1: '8',
                        outside2: '0',
                        outside3: '8',
                        outside4: '0',
                        uuid: '0001'
                    }],
                inspectItemsInReport:
                    [{
                        applianceType: 6,
                        brickStone: { status: 1 },
                        constructionType: 'Brick',
                        crown: { length: '24', material: 'Concrete', status: 1, width: '24' },
                        flashing: { status: 1 },
                        flues: 2,
                        height: { status: 1 },
                        id: 70232,
                        location: 'Exterior',
                        locationOnHouse: ['Right', 'Front'],
                        mortar: { status: 1 },
                        name: 'Chimney-1',
                        requiredPics:
                        {
                            pictures:
                                [{
                                    downloadLink:
                                    {
                                        aa: 2,
                                        za: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/front_of_house_0_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=52d57e18-2106-41c4-9199-145886a6a70b',
                                        w: null,
                                        ua: null,
                                        jb: null,
                                        od: false,
                                        vc: false
                                    },
                                    fileName: 'front_of_house_0_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'front_of_house',
                                    uuid: '0'
                                },
                                {
                                    downloadLink:
                                    {
                                        aa: 2,
                                        za: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/chimney_0_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=c9d4dc1c-d8ce-494f-81be-61790c97b863',
                                        w: null,
                                        ua: null,
                                        jb: null,
                                        od: false,
                                        vc: false
                                    },
                                    fileName: 'chimney_0_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'chimney',
                                    uuid: '0'
                                }]
                        },
                        roofMaterial: 'Metal',
                        structureIntegrity: { status: 1 },
                        stucco: { status: 2 },
                        uuid: '0'
                    },
                    {
                        applianceType: 0,
                        cap: { status: 1 },
                        condition:
                        {
                            comments: [3],
                            pictures:
                                [{
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=13535d5f-c00f-4831-b87f-1f52adb244c9',
                                    fileName: 'condition_0001_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'Condition',
                                    uuid: '0001'
                                },
                                {
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_1_-L4LsyR78Itb1XaqwpY6?alt=media&token=d29d3eca-827b-4a65-a57d-751283949d89',
                                    fileName: 'condition_0001_1_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 1,
                                    name: 'Condition',
                                    uuid: '0001'
                                },
                                {
                                    downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/condition_0001_2_-L4LsyR78Itb1XaqwpY6?alt=media&token=246b955c-9ff4-485a-8232-23f2ed7d4e88',
                                    fileName: 'condition_0001_2_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 2,
                                    name: 'Condition',
                                    uuid: '0001'
                                }],
                            recommendations: [0],
                            status: 0
                        },
                        id: 72331,
                        inside1: '7',
                        inside2: '0',
                        inside3: '7',
                        inside4: '0',
                        insulation: { status: 2 },
                        length: '32',
                        material: 'Terra Cotta',
                        name: 'Flue/Vent-1',
                        outside1: '8',
                        outside2: '0',
                        outside3: '8',
                        outside4: '0',
                        uuid: '0001'
                    },
                    {
                        applianceType: 3,
                        firebrick: { status: 1 },
                        fuel: 'Wood',
                        hearthPadProtection: { front: '18', left: '18', right: '18', status: 1 },
                        id: 9070,
                        ifInsert: 'n/a',
                        location: 'Family Room',
                        name: 'Stoves-1',
                        requiredPics:
                        {
                            pictures:
                                [{
                                    downloadLink:
                                    {
                                        aa: 2,
                                        za: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/appliance_0131_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=0247b70e-e546-439b-952e-0b9892fba65c',
                                        w: null,
                                        ua: null,
                                        jb: null,
                                        od: false,
                                        vc: false
                                    },
                                    fileName: 'appliance_0131_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'appliance',
                                    uuid: '0131'
                                }]
                        },
                        stoveClearances: { back: '18', left: '18', right: '18', status: 1 },
                        stoveCondition: { status: 1 },
                        stovePipe:
                        {
                            pipe_size: '6 in. Round',
                            pipe_type: 'Black - Single Wall',
                            status: 1,
                            to_ceiling: '36',
                            to_wall: '12'
                        },
                        thimble: { status: 1 },
                        type: 'Free Standing',
                        uuid: '0131'
                    },
                    {
                        applianceType: 0,
                        cap: { status: 1 },
                        condition: { status: 1 },
                        id: 49463,
                        inside1: '7',
                        inside2: '0',
                        inside3: '7',
                        inside4: '0',
                        insulation: { status: 2 },
                        length: '32',
                        material: 'Terra Cotta',
                        name: 'Flue/Vent-2',
                        outside1: '8',
                        outside2: '0',
                        outside3: '8',
                        outside4: '0',
                        uuid: '0202'
                    },
                    {
                        applianceType: 3,
                        firebrick: { status: 1 },
                        fuel: 'Coal',
                        hearthPadProtection: { front: '18', left: '18', right: '18', status: 1 },
                        id: 28571,
                        ifInsert: 'n/a',
                        location: 'Den',
                        name: 'Stoves-1',
                        requiredPics:
                        {
                            pictures:
                                [{
                                    downloadLink:
                                    {
                                        aa: 2,
                                        za: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/appliance_0231_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=1373e113-0062-43d5-a29e-9ad8eb43b618',
                                        w: null,
                                        ua: null,
                                        jb: null,
                                        od: false,
                                        vc: false
                                    },
                                    fileName: 'appliance_0231_0_-L4LsyR78Itb1XaqwpY6',
                                    id: 1517547600000,
                                    index: 0,
                                    name: 'appliance',
                                    uuid: '0231'
                                }]
                        },
                        stoveClearances: { back: '36', left: '36', right: '36', status: 1 },
                        stoveCondition: { status: 1 },
                        stovePipe:
                        {
                            pipe_size: '8 in. Round',
                            pipe_type: 'Stainless Steel',
                            status: 1,
                            to_ceiling: '18',
                            to_wall: '18'
                        },
                        thimble: { status: 1 },
                        type: 'Free Standing',
                        uuid: '0231'
                    }],
                requiredPics:
                    [{
                        downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/front_of_house_0_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=52d57e18-2106-41c4-9199-145886a6a70b',
                        fileName: 'front_of_house_0_0_-L4LsyR78Itb1XaqwpY6',
                        id: 1517547600000,
                        index: 0,
                        name: 'Front Of House',
                        uuid: '0'
                    },
                    {
                        downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/chimney_0_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=c9d4dc1c-d8ce-494f-81be-61790c97b863',
                        fileName: 'chimney_0_0_-L4LsyR78Itb1XaqwpY6',
                        id: 1517547600000,
                        index: 0,
                        name: 'Chimney-1',
                        uuid: '0'
                    },
                    {
                        downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/appliance_0131_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=0247b70e-e546-439b-952e-0b9892fba65c',
                        fileName: 'appliance_0131_0_-L4LsyR78Itb1XaqwpY6',
                        id: 1517547600000,
                        index: 0,
                        name: 'Stoves-1',
                        uuid: '0131'
                    },
                    {
                        downloadLink: 'https://firebasestorage.googleapis.com/v0/b/inspection-reports-555f2.appspot.com/o/appliance_0231_0_-L4LsyR78Itb1XaqwpY6?alt=media&token=1373e113-0062-43d5-a29e-9ad8eb43b618',
                        fileName: 'appliance_0231_0_-L4LsyR78Itb1XaqwpY6',
                        id: 1517547600000,
                        index: 0,
                        name: 'Stoves-1',
                        uuid: '0231'
                    }],
                comments:
                {
                    brickStone:
                        ['Brick face is falling off',
                            'Bricks/Stone is deteriorating',
                            'Missing bricks/stones',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    cap:
                        ['Mesh is too small; this is a flue obstruction',
                            'Damaged',
                            'Non-stainless steel cap is rusting',
                            'No cap',
                            'Cap height does not give minimum clearance requirements',
                            'Masonry cap with no screening',
                            'Cap with no screening',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    condition:
                        ['Missing mortar joints',
                            'Tiles are shifted/misaligned',
                            'Holes in the mortar joints',
                            'Broken tile(s)',
                            'Cracked tile(s)',
                            'Heavy creosote build up/ Glazed Creosote',
                            'Stone unlined',
                            'Brick unlined',
                            'Multiple appliances venting through same flue',
                            'Obstructions',
                            'Deteriorated/Rusted',
                            'Separated at joints'],
                    crown:
                        ['The crown is cracked',
                            'The crown is deteriorated',
                            'Crown is breaking up',
                            'Crown has hairline cracks',
                            'Crown does not have proper slope',
                            'Chase cover is rusting/deteriorating',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    damper:
                        ['Damper is severely rusted',
                            'Damper does not close properly',
                            'Damper is obstructed',
                            'Not aligned for proper operation',
                            'Damper is sticking',
                            'Damper is missing',
                            'Damper handle is broken/missing'],
                    firebox:
                        ['Mortar joints are cracked/missing',
                            'Firebrick are damaged or loose',
                            'Cracks or holes',
                            'Refractory panel(s) are cracked/damaged',
                            'Rusted',
                            'Surface is deteriorated',
                            'Trigger Danger Report'],
                    firebrick: ['Firebricks are cracked/damaged'],
                    flashing:
                        ['The flashing on the chimney is improperly installed',
                            'The flashing on the chimney is deteriorated',
                            'The flashing on the chimney is not sealed properly',
                            'There is no proper flashing installed on the chimney',
                            'The chimney does not have a proper cricket installed',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    hearth:
                        ['The hearth extension is cracked',
                            'The hearth extension must be at least 16” in depth and 8” on sides when the firebox opening is less than 6 square feet',
                            'The hearth extension must be at least 20” in depth and 12” on sides when the firebox opening is 6 square feet or larger',
                            'Trigger Danger Report'],
                    hearthPadProtection:
                        ['No protection',
                            'Hearth rugs alone are not adequate protection',
                            'At least18 inches of hearth is required around the stove',
                            'Improper hearth construction/material',
                            'Owners manual not available to assess clearance requirements',
                            'Trigger Danger Report'],
                    height:
                        ['The chimney is not tall enough.  The top of the chimney must be2 feet higher than anything within 10 feet and must be a minimum of 3 feet tall',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    insulation: ['Liner is not properly insulated'],
                    mortar:
                        ['The mortar joints have deteriorated',
                            'The mortar joints have structurally failed, causing the brick/stone to shift/fall',
                            'The mortar joints are cracked and deteriorated',
                            'Missing mortar joints',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    smokeChamber:
                        ['Missing mortar joints',
                            'Bricks/stones are missing/deteriorating',
                            'Visible holes',
                            'Trigger Danger Report'],
                    stackPipe:
                        ['Stack pipe is rusted/damaged',
                            'Stack pipe needs to be a minimum of 18 inches from combustibles per code requirements',
                            'Pitch is not appropriate',
                            'Stack pipe is not secured properly',
                            'Not sealed at thimble',
                            'Stack pipe does not terminated properly',
                            'Trigger Danger Report'],
                    stoveClearances:
                        ['Unlisted stove requires 36 inches of distance from combustibles.',
                            'Stove is not installed according to manufacturer guidelines',
                            'Owners manual not available to assess clearance requirements',
                            'Danger Report - Do not use (Trigger)',
                            'Trigger Danger Report'],
                    stoveCondition:
                        ['Crack/hole in stove',
                            'Door does not seal properly',
                            'Trigger Danger Report'],
                    stovePipe:
                        ['Stove pipe is rusted/damaged',
                            'Stove pipes are installed incorrectly',
                            'Stove pipe needs to be a minimum of 18 inches from combustibles per code requirements',
                            'Danger Report - Do not use (Trigger)',
                            'Trigger Danger Report'],
                    structureIntegrity:
                        ['Chimney is pulling away from house',
                            'Gap between chimney and house',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    stucco:
                        ['Stucco is deteriorating/falling off',
                            'Stucco has cracks',
                            'Stucco has no wire',
                            'Wood structure under stucco is compromised',
                            'We cannot state satisfactory due to not being able to visually inspect area'],
                    surroundClearance:
                        ['Mantel is loose',
                            'Mantel should be a minimum of 12 inches above the fireplace opening',
                            'Wood surround should be a minimum of 6 inches',
                            'Trigger Danger Report'],
                    terminationCondition: ['Flapper not functioning properly', 'Termination is missing'],
                    thimble:
                        ['Thimble is not a listed wall pass-through',
                            'Thimble is not constructed appropriately',
                            'Danger Report - Do not use (Trigger)',
                            'Trigger Danger Report'],
                    ventCondition:
                        ['Plastic Used',
                            'Gaps or holes in vent',
                            'Areas of downward pitch',
                            'Vent too long',
                            'Vent wrong size',
                            'Disconnected from dyer',
                            'Vent pipes are joined with screws',
                            'Vent pipes are not secured together']
                },
                recommendations:
                {
                    brickStone:
                        ['Tear down and rebuild',
                            'Replace damaged bricks where needed',
                            'Relay brick course(s)',
                            'Return visit needed with 48\' ladder',
                            'Return visit needed with lift rental'],
                    cap:
                        ['Install stainless steel cap',
                            'Install stainless steel screening',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    condition:
                        ['Install new stainless steel liner',
                            'Apply Cre-Away Pro',
                            'Maintain with Cre-Away',
                            'Apply Fireguard Ceramic Mortar Joint System',
                            'Apply Fireguard Ceramic Resurfacing System',
                            'Install double wall chimney system',
                            'Replace deteriorated/rusted pipe',
                            'Replace top piece of terra cotta'],
                    crown:
                        ['Apply crown seal to crown',
                            'Pour new concrete crown',
                            'Remove old crown and pour new crown',
                            'Install a prefab crown',
                            'Pour new concrete crown and raise up flue(s) with new terra cotta',
                            'Seal cracks with silicone',
                            'Install new stainless steel chase cover',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    damper:
                        ['Install top mounting damper',
                            'Remove obstruction',
                            'Repair damper handle'],
                    firebox:
                        ['Grind out and repoint where needed',
                            'Replace firebricks',
                            'Replace with refractory panels',
                            'Inspect yearly for integrity',
                            'Install stove with lining system',
                            'Resurface damaged area'],
                    firebrick: ['Replace firebrick'],
                    flashing:
                        ['Install new counter flashing that is to be cut into the chimney',
                            'Seal flashing with caulk',
                            'Apply Acrymax to flashing',
                            'Apply Acrymax and Fabric to flashing',
                            'Install step flashing and counter flashing',
                            'Install cricket',
                            'Install kick out flashing',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental',
                            'Building materials extends to shingles'],
                    hearth: ['Install proper hearth to meet codes'],
                    hearthPadProtection: ['Install appropriate hearth pad'],
                    height:
                        ['Extend chimney with brick, block, and/or terra cotta to meet code requirements',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    insulation: ['Insulate with thermal pour', 'Insulate with a blanket wrap'],
                    mortar:
                        ['Grind out and repoint where needed',
                            'Grind out and repoint roof line up',
                            'Grind out and repoint entire chimney',
                            'Spot point where needed',
                            'Grind out and repoint roof line up where needed',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    smokeChamber: ['Apply smoktite system'],
                    stackPipe:
                        ['Replace stack pipe',
                            'Install heat shield',
                            'Seal thimble',
                            'Install 3 screws per pipe joint'],
                    stoveClearances: ['Move stove or install appropriate clearance reduction system'],
                    stoveCondition: ['Replace stove', 'Replace door gasket'],
                    stovePipe:
                        ['Replace stove pipe',
                            'Install double wall pipe',
                            'Install heat shield',
                            'Install 3 screws per pipe joint'],
                    structureIntegrity:
                        ['Anchor chimney to house',
                            'Seal gap with silicone',
                            'Tear down and rebuild',
                            'Extend Footer',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    stucco:
                        ['Patch stucco',
                            'Wire, brown coat and stucco chimney',
                            'Rebuild wood chase',
                            'Return visit needed with 48’ ladder',
                            'Return visit needed with lift rental'],
                    surroundClearance:
                        ['Mantel should be re-secured',
                            'Mantel needs to be raised to meet code requirements of 12 inches',
                            'Wood surround needs to be extended to meet code requirements of 6 inches'],
                    terminationCondition:
                        ['Replace',
                            'Install cage or screen to keep nesting animals/birds out'],
                    thimble: ['Install appropriate wall pass through thimble'],
                    ventCondition:
                        ['Replace with metal',
                            'Replace with pop-rivets',
                            'Joints should be pop-riveted together']
                },
                signature: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABN4AAAEsCAYAAAAck36TAAAAAXNSR0IArs4c6QAAQABJREFUeAHt3Qn4JVV95nG0QUKQECRIkJDuIQQZhDA8qCEI00GQQRgjkUGIj0jHASEQfSQQDAwwOuiDIwaNMhECEpSA4Iii+IisIriyCsoiirTILgjI2s0y877CiYfynLp1761bt5bveZ5f136WT9W9Xff8a1lhBRICCFjgY4r/VyHu1zqrKUgIIIAAAggggAACCCCAAAIIIIAAAgggMELgIi2v0ul2wIh8WIwAAggggAACCCCAAAIIIIAAAggggAACzwlcrOGoTrdHtc5aiCGAAAIIIIAAAggggAACCCCAAAIIIIBANYEqnW5nVsuKtRBAAAEEEEAAAQQQQAABBBBAAAEEEEDAAu5QG3Wl25FQIYAAAggggAACCCCAAAIIIIAAAggggEB1gW9q1VGdbgdXz441EUAAAQQQQAABBBBAAAEEEEAAAQQQQOBOEYzqdDsQJgQQQAABBBBAAAEEEEAAAQQQQAABBBCoJrCKVrtaMarTbY9q2bEWAggggAACCCCAAAIIIIAAAggggAACCFjgMcWoTrftoEIAAQQQQAABBBBAAAEEEEAAAQQQQACBagJv1WqPKkZ1ur26WnashQACCCCAAAIIIIAAAggggAACCCCAAAIbiGBUh5uXbwQVAggggAACCCCAAAIIIIAAAggggAACCFQTOEurjep0W6Z1/Ow3EgIIIIAAAggggAACCCCAAAIIIIAAAghUELhM64zqdFuqdRZUyItVEEAAAQQQQAABBBBAAAEEEEAAAQQQQEACjytGdbrx5lIOFQQQQAABBBBAAAEEEEAAAQQQQAABBCoKrKf1fOtoWafbE1p+aMX8WA0BBBBAAAEEEEAAAQQQQAABBBBAAIHBC2whgbIONy97ZPBKACCAAAIIIIAAAggggAACCCCAAAIIIDCGwBu07qhOt3O1Di9RGAOVVRFAAAEEEEAAAQQQQAABBBBAAAEEhi1wtJo/qtPtY8MmovUIIIAAAggggAACCCCAAAIIIIAAAgiMJ/B1rT6q022l8bJkbQQQQAABBBBAAAEEEEAAAQQQQAABBIYtcJeaX9bptlzL1xg2Ea1HAAEEEEAAAQQQQAABBBBAAAEEEEBgPIHHtHpZp9t3xsuOtRFAAAEEEEAAAQQQQAABBBBAAAEEEBi2wE5qflmHm5ftP2wiWo8AAggggAACCCCAAAIIIIAAAggggMB4Aodo9VGdbpuMlyVrI4AAAggggAACCCCAAAIIIIAAAgggMGyBi9X8sk63R7V8lWET0XoEEEAAAQQQQAABBBBAAAEEEEAAAQSqC6yjVZ9RlHW6nV49O9ZEAAEEEEAAAQQQQAABBBBAAAEEEEAAgQ+IoKzDzct8+ykJAQQQQAABBBBAAAEEEEAAAQQQQAABBCoK3Kz1yjrdntTyBRXzYjUEEEAAAQQQQAABBBBAAAEEEEAAAQQGL7CBBH6sKOt0O3HwSgAggAACCCCAAAIIIIAAAggggAACCCAwhoBfjlDW4fawli8ZIz9WRQABBBBAAAEEEEAAAQQQQAABBBBAYPACX5dAWafbQ4MXAgABBBBAAAEEEEAAAQQQQAABBBBAAIExBNbQujcocp1uT2mZby3leW5CICGAAAIIIIAAAggggAACCCCAAAIIIFBFYDutlOtw8/zHFetUyYh1EEAAAQQQQAABBBBAAAEEEEAAAQQQQOBZgeM0KOt02xsoBBBAAAEEEEAAAQQQQAABBBBAAAEEEKgusJpWfUSR63TzraW+Eo6EAAIIIIAAAggggAACCCCAAAIIIIAAAhUFRt1a+qmK+bAaAggggAACCCCAAAIIIIAAAggggAACCDwncLGGuavcntGyxUghgAACCCCAAAIIIIAAAggggAACCCCAQHWBnbSqO9ZynW5fqp4VayKAAAIIIIAAAggggAACCCCAAAIIIICABa5S5DrcPH8rr0RCAAEEEEAAAQQQQAABBBBAAAEEEEAAgWoCB2u1sqvcvqPlC6plxVoIIIAAAggggAACCCCAAAIIIIAAAgggsIEIfqbIXeX2tJYtUpAQQAABBBBAAAEEEEAAAQQQQAABBBBAoILAKlrnc4pch5uvfjuyQj6sggACCCCAAAIIIIAAAggggAACCCCAAAKRwKMaz3W6XR6txygCCCCAAAIIIIAAAggggAACCCCAAAIIjBDwM9ruV+Q63NwZx3PcRiCyGAEEEEAAAQQQQAABBBBAAAEEEEAAgSCwlka+rrhHkep083PcdlGQEEAAAQQQQAABBBBAAAEEEEAAAQQQQKCiwEpa7ylFqsPtSc2/TOGOORICCCCAAAIIIDCugM8zjlU8qPD5xi2KXRUkBBBAAAEEEEAAAQR6LbCJWpfrcHMnnG8rXafXAjQOAQQQQAABBGYtsFQFpP6494FZF0z+CCCAAAIIIIAAAgjMQ2ALFXq7InUS7HnujCMhgAACCCCAAALTCmyrDHLnG57vN6iTEEAAAQQQQAABBBDohcAiteJ8RdkJ8DVazlVuQiAhgAACCCCAwNQCZyuHsvOO66cugQwQQAABBBBAAAEEEGiBwJtUh7ITX788YasW1JMqIIAAAggggEB/BK5TU8rOP7yMhAACCCCAAAIIIIBAZwV8W+kzirKT3o062zoqjgACCCCAAAJtFvBLFcrOQbzso21uAHVDAAEEEEAAAQQQQCAlsJlm+u1hZSe7P9HyrVMbMw8BBBBAAAEEEKhBwH/cKzsXCctqKIosEEAAAQQQQACB5gVWbL5ISpyzwBoq/xuKjUvq4Svg/knxdyXrsAgBBCYT8FWm6yv+q+LFitUUyxVXKe5SnKX4uYKEAAIIDEHg9yo2ckOtd3PFdVkNAQQQQAABBBBAAIHGBRaoxHMU4S/HueFljdeMAhHor4A71d6jGPXSktTn8Qlt9zXFdgoSAggg0FeBNdWw1Hdgcd6NfQWgXQgggAACCCCAAALdFzhOTSiewBann9Q6u3S/qbQAgbkL+Efk0Qq/kKT4OZtm+lLl927FKgoSAggg0CeBqt+NfWozbUEAAQQQQAABBBDogcABakOVk9kzetBWmoDAvAV2UwVuUlT5zE27zv0qZx/FygoSAggg0HWBUS95Ct+Z23a9odQfAQQQQAABBBBAoB8CvjXNt6mFE9Xc8G6ts0hBQgCByQT8rMQLFb5iNPc5m+V8X1V3kMK3kpMQQACBrgosU8WrfFfe3tUGUm8EEEAAAQQQQACBfggsUjN+rBh18uq/LO+vICGAwHgCvsJsV4U726peoTHq81jHcv9oXUdBQgABBLooUOWPheG7sovto84IIIAAAggggAACHRfw1S5+E2I4KS0bXpFpq9+mGG/3WGY9ZiMwNIEXqsG+jXSpIv6MtHG87G3Fqj4JAQQQaKXAg6pV1e9Uv92UhAACCCCAAAIIIIBAYwKHqKSqJ6sbZWp1UiaP2zLrMxuBIQisr0Z+WlH181VlPd/efZ3iZMXRioMVH1ScqfD85Yoq+eTWeUrbkxBAAIGuCTykCue+14rzv9q1xlFfBBBAAAEEEEAAgW4KLFa1qz5X6vgRTSye1MbTIzZlMQK9E/BVY74yNP4cTDJ+j/L4kGILxbjPYFtD2xyhWKoYt+xTtQ0JAQQQ6JKAvy/H+a7rUtuoKwIIIIAAAggggEDHBNZSfX+pqHKC+rDW26BC+8ryqrA5qyDQC4Gt1Qpf5Vn2eShb9l1t6+e/baOoO+2gDC9QlJUfL6u7fPJDAAEEZilQ9bwmfM+tNsvKkDcCCCCAAAIIIIDAMAV8BYw70sJJ56jh3mMw5W41dRm3jJEPqyLQRQF3To/zYO/4s3eNtn1Dg432lXN7KEb9SN20wTpRFAIIIDCtQPy9WmXct+mTEEAAAQQQQAABBBCoRWAl5XKVosqJqNe5ROFtxk1l+Y+bF+sj0AWBVVXJ7ynKjv3csvdru3lfcXF/Sd0/r2UkBBBAoCsC474l+kddaRj1RAABBBBAAAEEEGi3wIGqXu6Hf3H+01p3symaU8wvnp4iWzZFoJUCvnUzPsarjN+gbXw7aluSO9hz9fb3AQkBBBDoikDuu6xsflfaRj0RQAABBBBAAAEEWiiwRHUqO9ksLnMH3bTpNGVQzDdMXz9t5myPQEsE3Fl1viIc26OGfkvocYqVFW1MZfVvY32pEwIIIJASKPsuyy17Yyoj5iGAAAIIIIAAAgggUCbgq2lyJ5ip+T/R+uO+LbGs/FQZYV7ZdixDoAsC66mS49zOtF0HGlV2q2wHqk8VEUAAgV8JhHONcYYXYYcAAggggAACCCCAQFUBPyvqTkXVE84nta5ftlB3Kiu/7rLID4EmBbZXYWXHd7xs3yYrNmVZfolKXPd4fJpbz6esFpsjgAACYwnE313jjI9VCCsjgAACCCCAAAIIDFPgOjV7nJNM/9CeVSp7u+nFsyqUfBGYscAHlX+Vz9gFWm+SF5PMuPql2fuqvFzbDirdkoUIIIBAewQeVFVy32Vl89dpTxOoCQIIIIAAAggggEDbBD6nCpWdTBaXfaWhBhTLDdM8rL2hHUAxtQpUeZ6bryD1bahdTH72XPiMFoef6WKDqDMCCAxS4FG1uvgdVmV6n0Fq0WgEEEAAAQQQQACBUoFdtLTKyWRYx8+kavKWsVBualjaMBYi0CKBtVWXHypSx3E874gW1XnSqsTticfvmTRDtkMAAQQaFrhJ5cXfX2F81HM5b2y4nhSHAAIIIIAAAggg0GKBjVQ3XzUWTiarDA+eQ3uOLKnjkjnUhyIRGFdgU20w6vO1TOtsPG7GLV0/d7v6Iy2tL9VCAAEEigLf0YzU97avSE7ND/P85mkSAggggAACCCCAwMAFfAvbY4pwklhleNaczXJ1fGDO9aJ4BEYJbKMVcsdvmH/aqEw6tjz3ggV39JMQQACBLghcoUqG7+h4+HBmfrzOWl1oIHVEAAEEEEAAAQQQqF9ggbK8UBGfHI4af0jrt+EEsqye9UuRIwL1CByrbMqOXS97ez1FtS6X3NW07vgnIYAAAm0X+LYqmPr+vj0zP163qWfgtt2Q+iGAAAIIIIAAAoMSOECtjU8Kq4y/oUVC+5fUv0XVpCoI/ErAbyL1VaJlnzPfjuQXEfQ15a6q9bPuSAgggEDbBY5RBVPf4Zdo/i8zy8L6/v4jIYAAAggggAACCAxEYCe1M5wIVh1+oqU2ufpzgtvSHTbQaq2rduc6ncIxfLrWcedcn9Mlalxobzzcq8+Npm0IINAbgS+oJfF3Vxi/RvNvyCwL63hIQgABBBBAAAEEEOi5wCK1z1fUxCeBo8bPa7lJWf1bXnWqNxCBHdXOnynKjtUdBmLxpozDUNo/kN1MMxHorcDxalnqu/xazf9oZlm8/r69laFhCCCAAAIIIIDAwAXWUPvvUMQnf6PG/SymzTrg5jci5trSgepTxZ4LHFdyfPq4dYfc6j03iJvntyanPq/ukCMhgAACbRfIPaPzYlXcVyynvt/ied9sewOpHwIIIIAAAggggMB4An5xwqcV8UlflfE9xitmrmu7czDXpmfmWjMKH7LA+mr8OYrcsen5bx0g0MYZk00HaEGTEUCgewJHqMqp7/VLn2tKallxXvdaTY0RQAABBBBAAAEEkgKTvDjhY8mc2j+zeFIbT7e/9tSwbwK5zqVwXPqteFv3rdEV27O51gsO8bDPL5SoSMNqCCDQAYGDVcf4uyuM+8U5TlUe5/HsmvyLAAIIIIAAAggg0FmBxar5MkU4GawyDCeMXW20b4vNtXNRVxtFvTspcIFqnTsWPf/8TraqvkofkvGprwRyQgABBGYn4CuVU9/xn3muyLMzy+NtfEU0CQEEEEAAAQQQQKCDAuuoztcr4pO7UeOPa/1NOtjWYpX9DLtcW7ndtKjF9CwEfKvkzYrccej5XbqFexZGzvNURdHIV4iQEEAAgS4I5N4KH+4Y2EWNKH7HFac/1YWGUkcEEEAAAQQQQACBXwv4OW4nK4ondqOm9/p1Fr0YK2tvLxpII1orsKdqVnb83a/lfqkAaYUVvieEotV1wCCAAAIdEfCLYIrfYZ7+UFT/1PJ4nq/SJyGAAAIIIIAAAgh0RMCvpY9P5qqMn9iRto1bzbLbTf0XahICdQusogxvUpR97j5ad6Edz++BhNc7O94mqo8AAsMRyL2Z+V0RQdn/CWFZtDqjCCCAAAIIIIAAAm0U2ECVqvIA33CC5+EtCr/qvs8pbm887mfekRCoU2BdZRYfY6nxXesssCd5pTrI1+xJ22gGAgj0X2BbNTH1fe+XLoT0iEZS68TzXhhWZogAAggggAACCCDQPoFxn+O2XE3w89+GkOKT2uL4ENpPG5sR8BVaxeMrnv6plm/YTFU6Vwodb53bZVQYAQQiAf+hIP6+D+M7RuucllknrOvh4mh9RhFAAAEEEEAAAQRaIuDnh8QnbVXGt25J3ZuqRuo2tuDUVB0op98C56p54ZhKDYf+1tKyve9bc4sdb7eVbcAyBBBAoGUC26s+qe/+90T19LlXap143inR+owiULfAnyjDlevOlPwQQAABBBDos4CfT1bWoRSfyIXxo/oMMqJtwaA4/OGI7ViMwCiBu7RC8biKp/celcHAl6euFLxi4CY0HwEEuiXgF1rF3/thfL2oGb6NNMzPDe+M1mcUgWkFVlQGf6vYTvGfFT7u+vpMZzWNhAACCCCAQH0Ci5XVUkXupC01/5ta31eVDDmlXMK8IbvQ9skF/JkKx1BuuPHk2Q9mS9+KVfQ7azCtp6EIINAHAT9jt/g95um3FhqXWqc4r7AJkwhUEvhtrfWnCncCH634tGIHhY+vBxUvURyveI2ChAACCCCAAAIZAb8A4QuK4gla2fR9Wn+LTH5Dm/1Mid3QLGjv9AKjXqJwu4pYdfpiBpHDMWpl8Xtsk0G0nEYigEBfBKp2vBVvqy9+93mahMAoAXeuOV6m+IhiK0X4jfDXGvc5yGMKL3+bYksFCQEEEEAAAQRGCPgvpqmTs9y85Vp/yYg8h7bYbxYr8xqaB+2dXMAvSMgdS57PbRzj2V6U8BzacyjHE2NtBBBom4BvI039gc9/pInT1zRR9v+Hl5EQSAn4hWhvV/yh4lLFNxRnK3zM3K3YT3Gb4hUKr+vOYBICCCCAAAIIVBDYTuvcoxh1khaW+6TvkAr5DnWV4JQaDtWEdo8nkLuqIRxTe4yXHWtL4CpF8AtDYBBAAIEuCfh8LXx/xcP3FRqxTWa9eJvCJkwOUODFarOPlYUKd669X3GzwsfJjc8NPb674lOK1ymcfBUcCQEEEEAAAQQqCvj20NSP0fjErDj+lYp5D3m1olk8vWjIMLS9ksBqWis+Zorjm1fKhZWKAg8VXJ8qrsA0Aggg0HKB9VS/4v8Jnl6SqHdqvXje0J/JmyDr9awXqHUbKV6q+D+KQxXXKeJjwuPfV/gP7HspXqtwZy8JAQQQQAABBCYU2FfbFf+zLZu+fcJyhriZb1/LWT4+RBDaXFnAf0V+UpE7fvyjizSZQPH2rCcmy4atEEAAgbkJrK2SU/8/vCdRo9R68Ty/tZ7UXwHffuxnwL5b4ZcgnK+I9388fpGW/ZviMMWLFD7OSAgggAACCCAwhcCB2tYPQo3/wy0bf1rr8teu8cHLTMfPjS2GInCnGpo7dhYOBWFG7Sy6Xjajcvqera+S2UdxquK7ilsVVyi+rDhK8WqFnw+0voKEAAL1CixWdsXvMk+fkijmlsy6Yfvi7amJLJjVEQG/SfSPFLsoPqn4rMJXdYd9XRzer2VXKvyH4ncofMspCQEEEEAAAQRqEvAPouJ/vmXTfh04aTKBshOeyXJkq74LXKAG5j6PftECaTqBoq3f0EaqJrCVVvucomhYddpXcf5S8XXFaQr/2FtdQUIAgfEEfAVT6nPnz1Ux7a8ZqXXDvDOKGzDdCYEFquWrFK9UHKc4VxH2adnwdK13puI1it9RrKQgIYAAAggggMAMBFLPckj9J33xDMoeYpYpW89bOkQM2lwq4NtAcscLz3Qrpau0MPVj1Z0/pLzAylrkW5SWKnLHZl3z/Ya83RQkBBAoF3CHS+pzd3liM1+dmlo3zPtZYhtmtU/gZarSXyr8fXyhIuy/UcObtO6HFf6j+58pSAgggAACCCDQkMCojrebG6rHUIopOykaigHtHC1wiFbJHSubjd6cNSoILNE6ReMKmw1yFXe4lR2TRcdZTPuqjEWD1KfRCJQL+POZ+szdmtkstW6Yd29mG2bPR2BFFbumwlekHaHw7ft+FmnYX6OGfo7pKQpv+8cKEgIIIIAAAgjMSSB3q+nDqo9vSSDVK1B2klRvSeTWVYGFqnjuONmyq41qYb0/nXBuYTXnXqXtE06547Op+ceqTuvMXYYKINAOgbVUjdRn75FM9S7KrB/yyGzG7AYE/oPK8DPZ/kFxtSLsk3GG7pw7XOGXIfjYICGAAAIIIIBASwTilyssV538o4Y0GwHb5k6g/Mwj0rAF/IwrfwZTx8ibhk1Te+ufLjinbsuqvdAOZehbcb9RMEodl/Oc91PV7wMKbr0WAmnQAqnPoa92SqUNNDO1fpiX2oZ59Qq8SNn5e2s7xQmKSTvZvM/OV/yNwlfDv0RBQgABBBBAAAEEEJBAOLlNDQEaroAfjuw3iqWOC5+Yk+oVKDofUG/2nc5tR9W+6NOF6dtVb3dQv7DT+lQegfEFcp/PXE659T2fVK+Ar871c/j+VuFOMt/OW+Y/atlXtP2eim0U/gMJCQEEEEAAAQQQQCAhUHZStUZifWYNQ+Dbambq2Dh5GM1vtJW+jb5o7YeOk1ZY4eNCKNp0cfo8tWM9digCAxHIfUZzzffLS3LbbJTbiPmlAitq6SsVuyr+VfE9Rc54nPmfVT67Kf5MwVtHhUBCAAEEEECgbwJnq0HxycFVfWvgnNpT9lYxP1uPNDyBhWpy/FkL4zcMj6KRFt9Z8F7aSKntLmR3Va/sx3g4JuPhMm3zc8W5Cr98YUOFr9xcqPhnxbcUfs5UvE3T475d1g+fJyHQZ4Hc5yrX5iO0ILfNzrmNmP8rAZ/DLVT8leIoxa2KnOUk809Sfq9VvELB1btCICGAAAIIINBngSVqXOqE4cw+N7rBtqVsw7wGq0FRLRDYS3UI+z4eLm1B3fpYhfUT3kN+fp5vVfpgwiQ+FovjZ2j9rRVV0zpacW/FvoovKMZ5K1+x7EmnD1a5JAT6KpD7XOTa6wfv57ah4+1ZNXfYb6Lwd9fxiusUObNJ51+rPD+i+BMFL0IQAgkBBBBAAIGhCeR+GOUe1js0n2nbW3aSNm3ebN8tgbtV3eLx8EvN84OSSfULHKksY2+/zGKoyVeoPaWIPcrGfYtu3cm3Tvktfr5C7gFFWfnTLntQ+fO5EgKpdwK5z0ZZQ3+ghantLi3bqGfLfEWZO7xer3Dn/HmKlEld8z6p/N+tcIceCQEEEEAAAQQQyF6RQMdbPQdH8cd/fFLnH8Kk/gv4eX43K+J97/HH+t/0ubVwdZX8tCI2H+pbnD9ccIhNiuNf0LpN3/LkK/GOVnx3jHoW652bdicfV5cIgdQbgdyxXtbAk7UwtZ1vxe9b8uf9NYq/U5yqSP3BK2UxzbzrVc7/VrxK8fsKEgIIIIAAAggg8BsCSzQndcLBraa/QTXxjJRvmDdxpmzYGYGzVNOwv+Ph+zvTgu5VdK+CuTvhhpbWVIN/pIiPudy4n+vZlivEfGWcn0PnTrMbFLk6jzP/GOXDw8qFQOq8QO64L2vYBlqY265rn4sXqC1/qFis+B8Kv5DgAUWufXXP9/Mu/ccMv1hhoYKEAAIIIIAAAghUFuDlCpWpJlqx7MRv0UQ5slFXBD6hiqb2/5ZdaUAH6+krqIpXGPqlAENKm6uxqeOuOO9erfeulsO4Y+Cdijpe3vB15eMOSRICXRUofobD9Kj2hPWKw91GbTiH5b+jMl+u+O8KX0l2maJY7yamfVfCcYq3KRYqSAgggAACCCCAAAItFlikuuVOEp9scb2p2nQCvoIotd9/PF22bD1CwLeUxu73aHrBiG36tPgthfbHFvF4F6+4XE1ty3Vmx20bNf4z5cOzl/p01A+nLblje5SAO9lT2x4yasMZLF9RefoqvNcr/NbVLyv8pvdU/Zqad7vK/yeF32C6roKEAAIIIIAAAggg0EGBspPHDjaHKo8Q2FrLU/v8Ls1fZcS2LJ5O4HxtHtv7R90QkjsXz1TEbU+N+1apTXsA8mq14b4K7U0ZhHnf0vZ+BiMJga4IhGO3OBxV/xO1QnEbT/9o1IYTLvftoH+q+AfFpxQ3KVLlz2PeharLYYptFC9RkBBAAAEEEEAAAQR6IlB2cskPv57s5OeasbKGyxXFff6M5vmv/KTZCZyirGN3Pzx8/dkV15qcV1dN/CbPuO2p8cO1TtMvT5g1kh+mfnmFtqc8wrzFs64k+SNQk0A4ZovDUdn7lvLiNmF61Lap5b+rme7Af4fCVxnP63bQ0IbU0M+I/EeFbxVdR+Hnw5EQQAABBBBAAAEEeixwsNqWOjH0PHfIkPojcJ2aktrXfbjKqO176f6C/UfbXuEa6uerNvzyiNQxF8/rewekryR9j+KpChaxSxjfW9uREGi7QDhei8NR9V5bKxS3CdOp7wZ30Ps5a/9Z8b8Upyp8i3bYpk3DH6he7mD7a8XLFF17YYSqTEIAAQQQQAABBBCoS6DsRLWuMshnvgJHqfjUfj5rvtUaROkHJuzb8qbOWewAdzTljrf4GLxI6y2YRQVamudqqtdBitig6vgRLW0T1UIgCKSO5UfCwhHD1Lae5zeD+vlmvi2/Sid+Lp9Zz79Y9fOtq7sofKeAOwdJCCCAAAIIIIAAAgg8T8BXtuVOTHnu1/OoOjnhqwZS+/cnnWxN9yr9aMH/3d1rwlg1vrHQ3tSxt/NYOfZrZXfAva+CUdFtw34x0JoeCfiYLh6vnn6gpI1+hpmP6T0U7qBLbd+mederjscr/pviPylWVJAQQAABBBBAAAEEEBhLIHeCy9tNx2Js3cr+y3tu3/rZMqTZCqTeILvVbIucW+57qmS/gS93vHn+zYr1FKQVVvAzF9+rKPOKl/kPJH09dtQ0UocF/Cbe+FgN47do/qaKNyt8tef5irsVYXnbhteqbv+s8O3d/1HBHx6FQEIAAQQQQAABBBCoT6DsBLi+UsipaQE/xDm1b7dtuiIDLe+hgr8ftt+35A6kDytSx1mY504jP+ic9JsC7hz/mCJYlQ0f/M3NmYPAXAX83LKXKsqO2zYtu0x1/ZDCnYEbK/z9RUIAAQQQQAABBBBAoBGBshPjJY3UgELqFtheGab26+l1F0R+SYHU7Ve7Jdfs7sxVVXVfFZs6zsK8+7vbvEZr7mdDld32HzzPa7RWFIbAswLuIH6F4m2Kv1dcqmjjc9d8a/8XFYco/lLxMsWQniWp5pIQQAABBBBAAAEE2iqwtSoWftilhm2tN/XKC6T2o3/Yk5oR+ImKiffBMk336a12Swrti9vq8eUKX8nlDiVSdYF7tGrRsjjtZ8SREJiFgJ+95iui3Wn1TwpfNf2YongMznPat7D+i+LvFH+qWFdBQgABBBBAAAEEEECgEwJlJ9KdaACV/HeBT2gstT/9vB3S7AVSz9Y7cvbFNlaCb5lNHV9hnn+ouzOfNL6Ar865TREsc0Oe0Ti+LVs8K+BjbC2Fn83mzit3sH1FkTvW5j3f9VxbQUIAAQQQQAABBBBAoPMCZbeN+CHxpG4I+AdV6ofSad2ofi9qeWFiH/Thqoz1E+0qHmtnaR13PJImF1hPmxZdi9M/njx7thyQwB+orb6C7S8UJyjuVBSPpXlNX/xcnd6soZ8BmauHFpEQQAABBBBAAAEEEOiHwCI1I3fi6/mkbgh8U9VM7cdu1L4ftSz6X9qDZo16gYLbvEMP2tmWJixURYrHUXHaHaEkBCzwW4rtFHsqDlT4O6d4vMxj+nuqxzGKdygWK/ym0FTHvJ8XmavfhlpGQgABBBBAAAEEEECgNwK5E1/PJ7VfwFdVpfbh/u2vem9qeFRiH+zW4datobo/kGhTfJz5gf/+4UyqV+Dzyi52Lo7fVG9x5NYBgReoju6IOkDxHsW/Kvw8xeKx0eT045nyNXuslKvzSWPlwsoIIIAAAggggECNAj75IiFQt4BP4HMPgN9Gy75Rd4HkV6vAj5TbBokc+b5IoMxo1hPKd+Uob9/CvWI03aXRxarsJSMq/Fot/9qIdVg8uYA7I8oSn+0yne4u81VhfjPy6xQvV/j24z9/blyDxtNdKvFaxQ8UFyn8tmJ3/D6syB2j4x6b9ymvNRXF5BfT+Io+EgIIIIAAAggggAACvRHwSXQuetPIHjbEnT2p/bZLD9va1ibtkNgHXX223hmJtsTH11Itd8cAabYCqSso4/3wxtkWT+4NCLhj3n/Y+nvFEYqvKuJ93OT4bSr7C4q3K3ZUuNNvVErVb5I3aL9FBaXy8jwSAggggAACCCCAAAK9Esid+HLy2+7dnLot7el2V7l3tbtBLSp+frrWOeVbRn+RaEfcrrf2bs+1u0GxfXH8++2uOrXLCPyR5n9EcaeiuE+bmPabid+r+BvFaxW+omzSK3NT9b1e+U2SUnl53uqTZMY2CCCAAAIIIIAAAgi0VaDseTG+CobUToHUD5bd21nVXtbKt0gV98HSjrXUV9sU2xBP36LlCzrWpj5U9+6S/eJbm0ndEnAnUtk+jT9z04w/qXL8xwC/zfQvFK9UrKeoM+Xeon3OhIXk2nvkhPmxGQIIIIAAAggggAACrRXInfx6Pql9Av5RUtxn7kAlNSfwcRVV3AdLmit+qpL8AgX/OC/WP0z7ykmucpuKeKqN/SD9sC9SwxdOlTsbNy1Qdktlav9WmfclNeIYxcGKjRW/o2jiuNhJ5aTqN+kffU7P5OdOfxICCCCAAAIIIIAAAr0SSJ1Ih3m9amhPGvOI2hH2Txju15O2daUZqTf75V5U0qY27azK+HlM4bgpDn+sZU38gG+TSdvqknt+Y9hXu7atwtSnVGCajrdvKefDFb5F1J1evjV8nunrKjwch/FwlQkr5T8CxPnE4xNmyWYIIIAAAggggAACCLRT4DFVKz7hjccva2eVB1srvzwh3j8en+TB1oMFrKHhCxP7wB1WbU6+3c1X6aU6bcPx5JdFkNohUNY5+uF2VJFaVBSocqvpzcrraMVfKd6m+ENFG5NvdQ7fF/FwmrrG+cTja06TKdsigAACCCCAAAIIINBGgfiEtzjexvoOtU6pB/p/cKgYc2r3pSq3+Bnx1ShtTb6C6l5Fsc5h+gwtW6etlR9ovXK34Hmf3TZQky43O7xc4Q414mTF2xX7KrZQdOkK0/CdEQ+nfe7gdTKI8wvjXMUtGBICCCCAAAIIIIBAvwTCyW5q2K+Wdrc166rqqf3jjhVSMwK+nTS1D5opffxSTsnUN7ThXeNnyRYNCPiWwrCPUsMuddY0wEURDQjkvvvOm7LsHbV96hi/cMp82RwBBBBAAAEEEEAAgdYJ+CqK1Mmv513eutoOs0KXqNnFfeRn7pCaEzhURRX3wcXNFV+5pI205pWJuoa6n6Bl7sgltVfAL0wJ+6s43Ly91aZmPRU4KHM8blxDe4vHt6d9WzwJAQQQQAABBBBAAIHeCaROfsO83jW2gw36heoc9kcYLuxgO7pc5dQzjjZsUYN8JdTeinB8pIYfa1F9qUpewA/WT+0/z/PteSQEmhTwm0ZTx2MddfAf91J5d+GFNXW0nzwQQAABBBBAAAEEBiSQOvEN8wbE0Mqmvk+1CvsiDO9rZU37Wyk/By3Yx8O2tNhvCLwnU0fX9yEFtygKoSNpJ9UzPs6K4x1pBtXsicCjieOxrqvSfAVn8fj29JE9saMZCCCAAAIIIIAAAgj8u4Cf1ZI6+fW86/99LUbmIfCgCo33zTOadkcQqTkB39Yb7wOPf6i54ktL2jlRt7iufhOmnxtG6o6A34YZ78Pi+OLuNIWadlzAHfbF48/Tx9XYrlT+p9WYP1khgAACCCCAAAIIINAagdTJb5jXmkoOrCJrq71hH4ThUwMzmHdz/QKLYB8P512vBarAtZm6uZ5PK3ZQkLopEB9rxfGru9kkat1BAb+EpXj8edr/N9WVUrdW315X5uSDAAIIIIAAAggggECbBFIn12Fem+o5pLp8X40N+yAMzxwSQAvaekpiHyybc702SdQpHB8eflXBM5LmvJOmLP5z2j7ep8XxKbNncwQqCZyutYrHnq+6rjOtpsyKZXh6/ToLIS8EEEAAAQQQQAABBNogcJYqkTr59bzH2lDBAdYhtT8GyDDXJqf2gW/vnEfybV+fVKTqFOYtmUfFKLN2Ab+dNuzT1LD2AskQgYTAzzSvePxdmlhvmlm521nreGvqNPViWwQQQAABBBBAAAEEZiJQPMGOp2dSIJlmBQ7Vktjf43dn12bBLAT2UKbFfeDpeSTf2uUr7VL18Ty/eZCr3ITQo5Tb156/V4/aSVPaKZC7zX67GVQ3dbvplTMohywRQAABBBBAAAEEEJi7QNkPvVXmXrthVeBhNbe4P/YbFsHcW1v09/Q8XqqwS+JYiOv20blLUYFZCMT7uDi+dBYFkicCkUCu4+1V0Tp1jea+4+rKn3wQQAABBBBAAAEEEGiNwCdUk+IPvDDth7WTmhFYQ8UE93jo+aRmBPZVMbF9GPdtUU0ld3ZfpghlF4dPapmf90bqp8A31aziPo+n+9lqWtUWAf+RIT7ePP6LGVXOt5UWy/K0b7kmIYAAAggggAACCCDQO4HUyW+Y17vGtrRB71O9gnkY0vHZ7M4K7vHwvAarsGPiGIjr8qkG60JR8xHwW2njfV4cn0+tKHUoAvckjr9vz7Dx/kNC8Ri/YoblkTUCCCCAAAIIIIAAAnMTKJ74xtOL5larYRV8jZobu3v8M8MimGtrj034ex/47XuzTgtUwEmK4v6Pp7eddSXIvzUC8X4vjremklSklwLF483TfgnTrNKXlXGqzFmVR74IIIAAAggggAACCMxN4GCVnDr5DfPmVrEBFRys4+GuA2r/PJu6UIXH7mH8hAYqtZXKeCpTvuvhh437uUuk4QiE4y81XGc4DLS0YYGtVV7qmFtvhvXIXeG5eIZlkjUCCCCAAAIIIIAAAnMTSJ1wh3lzq9SACg7W8dBXQpFmL3Cfiojdw/gsS/az3I7JlBvKf9MsK0DerRV4vOS4eHVra03Fui7w4cxxN+uO//B9Fw9neXtr1/cT9UcAAQQQQAABBBDosEB80lsc91/CSbMTWFVZF809TZq9wAdVRMr+rTMsel3lfX+mXNflNsXaCtIwBa5Ws1PHpOcdMkwSWt2AwFUqI3XczbromxPlPjPrQskfAQQQQAABBBBAAIF5COykQlMn3WHePOo0lDJz9kNp/7za6ds8w/EdD5fOsELHZcoM5e8+w7LJuhsCh5UcI74qiYTALATCd1A8vGMWBRXyPFzTcZlhnDd6F6CYRAABBBBAAAEEEOiHQDjhTQ370cJ2tmIfVQvzZveNrzpLmXuebwOtO22jDP28tlyZ52rZmnUXSn6dFPBxkDtOzulki6h02wV8O2nqmPtMAxV3B1uq7NMbKJsiEEAAAQQQQAABBBBoXCB18hvmLWm8NsMpMPVsnaeH0/zGW+pn5z2iCMd2PJzFc9W2VFlPZcpbpvlvV5AQCAIraSQ+JuPxb4SVGCJQo8COyis+zsL4fjWWUZbVk5nyy7ZhGQIIIIAAAggggAACnRTYTLUOJ9ypYScb1YFKn5Vwd4cMaTYCqWcK+Xj/Us3Fbaj8ch18Lu+Kmssju/4IPKqmpL6DL+tPE2lJiwT+OXO8NXUV7vGZ8tdqkRFVQQABBBBAAAEEEECgNoHUj70wr7ZCyOh5At/RVDAOw/uetwYTdQl8K2Ft83vrKkD5rKbwG0vvVIT9GQ8f1nxeniAEUlbgp1oSHzNhvM7jNFs4CwYn4D/0hGMsHjYFkbu9+tNNVYByEEAAAQQQQAABBBBoUsBvE4tPvOPxg5usyIDK+mHC/PYBtb+Jpr5QhZydcPbx7WPezziqI/k2Vu+7+HMTj5+gZXS61SHd7zy+X3IM9bvltG4eAvF3VDzeZF3icuPxJutAWQgggAACCCCAAAIINCKQe9BxOBFupBIDK+SXam/wDUNfBUeqR8DPzLpOEWyLw03qKWaFM5RPruPaV5RsX1M5ZNN/gY+oicXjNEz3v/W0sEmBVTPH2tVNVkJlfTJTj4UN14PiEEAAAQQQQAABBBBoRCD8wEsNG6nAwApZrvYWrS8YmMEsm5u7bc/m69dQsDvublAU92GY3kXLfMUdCYGqAqkXroTjyc8OJCFQl8C7lFE4tuLhB+sqoGI+q2fqUfezNytWh9UQQAABBBBAAAEEEJitgN+oGZ+Ax+NHz7boQeae6ni7eJAS9TZ6I2VX1um2sIbi9lQe8ecjHr9Hy/auoQyyGJ7AHmpyfCzF45sPj4MWz1Ag97KZebzYID7O4/EZNp+sEUAAAQQQQAABBBCYn0B80lscn1+t+lnyk2pW0Xi3fja1sVZtmTANxr4dtI7Oi+tLyripsZZSUB8F1is5tvwGShICdQmE78XisK78x8nnUK1crIenXz1OJqyLAAIIIIAAAggggEBXBFInv2FeV9rQhXrupEoG13h4eBcq39I6vjVjat/HFdO+3MDPaks9ly/svyVazq2lQiBNLODnEobjqTj0bc0kBOoQyD3T1d9v80rF493TPPN0XnuDchFAAAEEEEAAAQRmKuCHwadOgD3vEzMteViZ0/FW3/72m0lzby71cXu3wh0a06TcFRnO/0HFdtNkzrYIRAK57995dopE1WO0BwKnqw2p42yvObYtd+4xxypRNAIIIIAAAggggAACsxNInZCHebMrdVg5H6LmBtN4uNqwGKZurR8474612DAe98Pq3TE3aVpFG35TEecZj3MV0qSybJcTiI+v4nhuG+YjMI5A8bgK0+PkUfe6fi5mqEc83L3ugsgPAQQQQAABBBBAAIE2CMQnvcXxNtSvD3X4vhpRtPX0NJ1EfXAZpw37ZgyD60fGySyxrt98GvJKDT+Q2IZZCEwrkDrWwrxp82Z7BDYWQTie4uFtLaCJ6xPGr2tBvagCAggggAACCCCAAAK1C5Q9x4rbTevh9oP+ww+LeFhP7v3OZRM171JFztBv5113SoJdtX28X4rj206ZP5sjkBMoHmvxdG4b5iNQVeByrRgfU2Hcb9Sdd7pQFQj1iYerzrtilI8AAggggAACCCCAwCwE4pPe4vgsyhtankXTMD00h3Ha6x9fhymCVWroZ735zZCTJr8coex5cX4T7cJJM2c7BCoIpI7rMK/C5qyCQKlA7g8W/u6bd1pHFQjHejw8YN4Vo3wEEEAAAQQQQAABBGYhEJ/0FsdnUd6Q8vRz3Iqmnr5zSAhjtnUrrZ/7wRgsjx0zz+LqftPfE4qQX3HoK0X8zDcSArMUKB53YfqnsyyUvAchkHuO2mda1PqHVJdwzIchz9Js0Q6iKggggAACCCCAAAL1CdyrrMJJb3E4bQdHfbXsZk77Z2y/3c3mzLTWvgLinoxXOC6XavkWimmSbx0N+aWGx0yTOdsiUFHAHcyp48/zzq2YB6shkBPIPUZiYW6DOcx/p8pMfQbWnENdKBIBBBBAAAEEEEAAgZkLpE5+w7yZF97jAk5V24JjPORh/b/e6X5G280Zp9jsa1pnrV9vNvbYAm3xeUWcZ3F8z7FzZQMEJhMou5X66MmyZCsEfiXgW/WL322ebtsffPydnKrn4l+1gn8QQAABBBBAAAEEEOiZQOrkN8zrWVMbbU6uQ2mvRmvRzsK2VLXuU4TjrGz4himbsF2FcvwiBxICTQmcqYJyx/y0V3U21QbKaafArZlja9rv0Vm0NvV/5C2zKIg8EUAAAQQQQAABBBCYt8AdqkDuRyBXX0y+d+7KuO48eZad39JXlY16hls4Fv2sq42maPHK2vZKRcgvNbxCy33lBQmBJgVyt/j7s0FCYFKBhdow9T33nUkznPF2ueeguh0kBBBAAAEEEEAAAQR6J5A6WQ/zetfYhhp0t8oJhvHQz7YZSvIb9NzZdr8iNhg1vkTrT5rc4Vb2xtJQ9kGTFsB2CEwpEI7B4nDZlPmy+bAFjlfzi8eUp3drMUvqDzGvanF9qRoCCCCAAAIIIIAAAhMLpE7Ww7yJMx34hter/cEwHn69xy6+guFgxVcVqR9UsUNq/DPaznlMkvwm0o8pUvnG8/xG040nKYBtEKhBwC8SiY/HePzGGvIni2EK+Bbl+FgK4357qP8A0tZ0iSoW6hqGD7S1stQLAQQQQAABBBBAAIFpBMpuN/XbOUnjC3xcm4QfEvHwyfGzmvsWK6kG6ys2VOyjOFThzrUbFLk36MVtHjX+beXjvCdJfunCRxSjyvDywycpgG2eJ+Dj4CTFZYrliqcV7mT1+COK8xX7Kibdn9q01+kQtS53rH6q1y2ncbMUuESZp46rzWdZaA15+/+WVL23riFvskAAAQQQQAABBBBAoHUCqZPfMK91le1AhXZXHYNfPGzDc5y2VN22URylOEtxqeJOha+OcMdgXN9ZjrvjbtIfhptq29Mq1tXPi/MbVEmTCfg5eIcpxj0W/CyzYxV+0yLpWYGLNMg52piEwLgCvoI3dUz5u7zNV7uFdqb+z/lCWMgQAQQQQAABBBBAAIE+CaRO3MO8PrWzqbasrYKCX3HYxA/srVS+rzzy1WA/UrhTrViPeU27Pu44Gzf5R6SfGeeOwqp1H/LLLMb1Ta3vN75WtS5bz7dYL0oVMLB5fo5bzsm3S5MQGFfgWm2QOqaa+H9m3Lqm1j8xU39efJPSYh4CCCCAAAIIIIBApwXuUe1TJ++ex20fk+3aBzOmP58su+RW7oxy59K7FKco6rj1M3ccTDv/AtVvkWLc5NsWj1TcpahahyVatwtXe6iarU3bqmZVvauu9yHluXprWzz7ipU5zb50SuibQO5qtzZcWV3VegOtmPpcLKmaAeshgAACCCCAAAIIINAlgdTJb5jXpXa0pa7fUEWCX3Hoh6xPmtwR9WlF7s2pxbLmOf051dNXTY2bfMXg+xXevmr9n9S6viKODjch1JAeUB5V7cddz/t3aGlHNbjMaWgetHd6gdyVzJ+dPutGc1iu0oqfDf8RiYQAAggggAACCCCAQO8Eiie+8XTvGttAg96oMmLDePyKMcv31UdXl+QX5z2vcf94ch39fLtJbpvzrUXvUfjZb+O0wevvpCDVJ7CqshpnH0yyrl80MKT0YzU253TxkCBoay0CvhI9dzx17Y8PfhZkqi2+oo+EAAIIIIAAAggggECvBHK3RvqEeJKOlF7hTNiY1I+JMG9RxTzX03pPK8J28xo+rjrcp/AzhU5V+O2mvppt0h953m43xXmKcW4jdfvt4Svi+GEmhBkk377cxHF2psqZ9PiZQbNnlqXbWObpZzKSEBhHIHc8XT5OJi1Z12+nTrXn+pbUj2oggAACCCCAAAIIIFCrQOrkN8yrtaCBZHa+2hn8UsMqHUfHjcgjle+oee7oullxtuKTigMUuyg2UvhH0CzS9srUnWW+Qs2deKPqmFp+jrbbR+ErskizE/BViyn/eN5TWsdvw31Y4fF42Tjjt2rbvqdPq4FlJn1vP+2rV+CYkuNp5XqLaiy3n2XaxHd9Y7uAghBAAAEEEOiPwAv60xRa0lMB/zjMJY7fnEx+/hpa9Iv84l8tuUP/uhPsq4oTFH5uT5zc8eaOsXHTjdrgUsXPFZ9XXKfwlWJV00Kt6B89Tyher/ihwre8utPsDxS/rfCD8l/83NDTv6PwNv7xV9dVko8or39UuNPtKgVp9gLef97vZWk/LfTxGtKaGnmP4iDFuFex3aJtNlD0NZV9r7rzct2+Npx21S5Q9n/KhSrtdbWX2EyGfgbiuYmi/L3/F4n5zEIAAQQQQAABBLICdFxkaVjQEoFlqseLMnVxx4o7XUjjCVyp1bcYb5NfOfuqtK8r/JDpdyly3x++2sidav+iuEbh23M877cUr1SspPgThX+w/a7CnWbLFe7ouFXhzrjXKHyrsdd5qaIN6SZVwh07Do675vfIV1SkO1zL0r9p4Z6FFdwB96+KNxTmj5r0j+6dRq3UweXuUP8vJfV+uZa5452EQBWBh7WS/9iRSv6/2y+Z6WrKdVDn/u/rajupNwIIIIAAAggggAAC2Vui3JlDGl8g9/wa/8ggnm9wv0w+qujz1U/jH0Hz28KdvqOO0fdkqrdDhW2LeS/J5NXV2e7ILrYxnn6oqw2j3nMRKHvswN5zqVG9hV6i7OLPRxjfud5iyA0BBBBAAAEEEEAAgfkLhJPd1HD+tetmDfZVtVOezHv2Cg1f0bd2N3dtr2u9mlpXpfPNV8elkm9FdufSOMe5O6v6kr6vhpS1fVFfGko7Zi7gq0Fzx5KvVO9DynVUP9OHxtEGBBBAAAEEEEAAAQRiAd96mDvBr+u5XXF5Qxk/oMQ1593X+RfL4gjFrF7kMJRjqol2+pl99yhGHYufzFRmgeafWWH7OP9MVp2a/b4RbT6pU62hsvMU8DMA489HcXydeVau5rKXZtrqW9hJCCCAAAIIIIAAAgj0RsCda8UT+zDtB92TJhfYQptWuYIoeHd96OcRXa04WbFY4U4cUjcF/KKOUcejn9OWS+5oHbV9WH54LpOOzN94RFv9PMVxX0DRkaZTzZoFfJz4RSfhs1Ec+mrqPqVt1JhiGz19UZ8aSVsQQAABBBBAAAEEELBA6sQ3zENoeoFdlMV3FL6FJrjOa3i76nC34kuKjysuUbjj4zCFr9rZWrG/YpHCz+3ylQfuWFhZEa5Y81VNpP4LnK0mjjpOLyhhOL7C9iH/rnbS5m6XC+3yw+/9GSIhUEXgOq0Ujp3isOyzViXvtq5TbGeYbmt9qRcCCCCAAAIIIIAAAhMJhBPd1HCiDNkoKeCrC92p5SuFblSUXdmQ2hfjzHv8uTLcweaOv1cp/AwuEgLjCByolUcdd18oydDH+ajtvdzHa9fSJqrwqLZ9tmuNor5zEzi05Hjy/xV9vWoy10H/1rntCQpGAAEEEEAAAQQQQGAGAjspz9wPyNtmUB5Z/lrAP6Z2VnxOcYXiMUVuX6Tm+yo676PTFLsrFilICNQpcKIySx178bz3lhQYr1c23pU3NfolFO5sLGuLl7kzcSUFCYFRAm/XCmXHk4+5vqbc4y6W9rXBtAsBBBBAAAEEEEBguAJlJ/3DVZl/y30Lnm9V8y1t6z5XHXfWkRBoUuA4FVb2HeFl78xUyLcpj9o2LG9zB4M7CI4coy3cYpo5IJj9PIGtRhxT6z1v7X5O3JAx6NOLJPq552gVAggggAACCCCAwFgC4YdvajhWRqyMAAK9FKhy26hvpU6l7TUz9d2Smpfafp7z3OF2gMJXsKXqm5rn5yWSEBgl4Nv//RzA1DHkef7cDCFtpkamDD40hMbTRgQQQAABBBBAAIHhCHxCTU2d+Hqeb2MkIYAAAveIIPc9EeZ/IMN0QoVtncc5me3nMXuhCl2mCG2rMjxlHhWlzM4J+OrOHypyx9QhnWvRdBVOfc7c2U1CAAEEEEAAAQQQQKBXArkfAJ5PQgABBHzrc9n3RFjmW8e8bjHlbikL24XhEcUNG572baKfUYT6VB1+sOF6Ulx3Be4oOb58heXQkq9uS33O/GIgEgIIIIAAAggggAACvRFInfSGeb1pJA1BAIGpBLbU1uF7YdRw10JJfj7hgxW336OwbROTfhnCYRXrV2z7Dk1UkDJ6IeCr2YrHT5i+thctHL8RfpZdMIiHudvXxy+BLRBAAAEEEEAAAQQQaIHAxapDfMIbj1iXOwgAABWMSURBVB/cgvpRBQQQaIfAW1SN+PuhbNy3kLmzLiQ/M225omybsGzHsFEDw30q1inULQw/rO3cYUdCoIrAWVopHDvF4Ve0bEGVTHq6zt0Jmwc0jxcK9XSH0ywEEEAAAQQQQGCoAsUfAvH0UE1oNwII/KZA2VU78fdGGL9PWeyu8I9oxy8VYVlu+IzW+YBiVmmhMv6iIld+2fzztJ23JyFQVWCxVswdU49o2dA7cHMvWXhjVWDWQwABBBBAAAEEEECgCwK5HwWeT0IAAQRigTdoouw7I7fsDm13huL2ittfovXWUKylmCbtrI2PV5ymeEKRq1/Z/Ke13dYKEgLjCPjqzdxxdbOWrTNOZj1dd+2M0VE9bS/NQgABBBBAAAEEEBioQNlbCwdKQrMRQKBEYAMtc2dUrlNh1vN9VdyTCl8x5I68O58LX2HneU8p6qrDMcrLHYAkBMYR2E4r545BH7/+DJGeFbhVg6KVO8mHfAsuxwYCCCCAAAIIIIBADwWKJ71h2s9qIiGAAAJFAd86eqwifFf0bXij2kbnSHGvM11FYKFWKvs8bF8lkwGts0/Gy52XJAQQQAABBBBAAAEEeiNQ9iOhN42kIQggULuAb5e7QlH2HdK1ZXvVrkSGQxFwZ23Z8X7oUCDGaOf6GbPPj5EHqyKAAAIIIIAAAggg0HqBx1XD3I+F1leeCiKAwNwFfDvmrYrc90gX5p+q+vtKPhICkwhsqo3KjvOjJ8l0INvk3AbSfJqJAAIIIIAAAgggMBSB3InveUMBoJ0IIDC1wMbK4UuKeT4DLvddlpt/ouo77UscpoYjg04LlL1IwcfdhZ1u3ewr/1kVkfp8bjj7oikBAQQQQAABBBBAAIHmBFInvWFec7WgJAQQ6IPAymrEOxWXK8L3yLyHfqi9H9q+VHG2gjeVCoE0tcDhyqHs2L5u6hL6n8GijOHF/W86LUQAAQQQQACBcQVeMO4GrI9AiwT8wyGXOLZzMsxHAIEqAitppW0Vqyh+X+EOsBsU7qC7VrGm4gHFcsUHFAcqpk13KYOrFfcqzlf8X4WvxCMhUJfABcqo7GUJP9FyX7XFcTdaPHcOwvnHaDvWQAABBBBAAAEEEOiIwP6qZ+6v9n5+EwkBBBBoSsDPWntQkftOSs33M9r8cHt38pEQmKXAAmXuDt3UcRjmXanlPDOw+l7I3Z6OYXVD1kQAAQQQQAABBBDogED4wVAcPtWBulNFBBDol4B/cB+hKH4fjZreqV8MtKZlAqOe5+bj84yW1bkL1TlFlUx9tpd0ofLUEQEEEEAAAQQQQACBqgKpk94wr2oerIcAAgjUKbC+MvOtqeG7qMpwmdbfrc5KkBcCEjhYMer4exdSEwlslrG9ZaLc2AgBBBBAAAEEEEAAgZYK3KZ65X5UcLtpS3ca1UJgIAL7qZ2576fcfHfA7TAQH5o5OwH//3ezInechfnbza4Kg8g5OBaHg2g8jUQAAQQQQAABBBAYjkDxhDdMPzwcAlqKAAItFfDz285RhO+lqkM/L87PfyMhMK7ANtog9/yxcPzdqHX8ghDSdAI553Wny5atEUAAAQQQQAABBBBol0D4IZEatqum1AYBBIYq4Oe/fVaR+p4qm3e5tvG2JARGCfiNu0coyo4nL/vkqIxYXlngXK2Z8j6+cg6siAACCCCAAAIIIIBABwTuUh1TJ76eR0IAAQTaJOBbAL+qyH1n5ea70463n7ZpT7avLtdVOK42al+1O12jDTPmP+90q6g8AggggAACCCCAAAIFgVU0nfuxelJhXSYRQACBtggcroo8o8h9fxXnP6B1P6RYTUFCIAi8RSO3KorHSzz9NS1fK2zAsFaB2DkeX73WUsgMAQQQQAABBBBAAIE5C8Qnu8XxOVeN4hFAAIFSgTdq6ThvQV2u9ZcofGshabgCC9T09yqK/+fF0z6uDlGQZidwpbKOzcP4ibMrkpwRQAABBBBAAAEEEGheIJzopobN14YSEUAAgfEFXqVNqryJMnzPPan1dx6/GLbogYCf+3ePIhwLqaGXk2YvcICKSPlfP/uiKQEBBBBAAAEEEEAAgeYEDlZRqRNfzyMhgAACXRJYT5U9WZH7TivOv1vrbtulBlLXqQR20NbudC0eB2Hab9r8sIJbHYXQQLKzzYN/PNyigfIpAgEEEEAAAQQQQACBxgTik914fGljNaAgBBBAoD6BNZXVkQp3rMXfabnxm7QeD88XQo/Tnmpbbv+H+e/ucfvb2rSLMvvli22tMPVCAAEEEEAAAQQQQGASgfCjIzWcJD+2QQABBNog4LeZHqZ4WJH6fivOu13r+e2ppP4I+CVC31MU93U87auuNuhPkzvVEr/0JN4XYdz7hIQAAggggAACCCCAQG8E7lVLwslucdibRtIQBBAYtMBRar1frlD8jktNu6PGV82Rui2wSNVP7d943te0Dm8tnd9+tn28P+Lx3eZXLUpGAAEEEEAAAQQQQKBeAV8REJ/sxuM85Lhea3JDAIH5CnxSxcffcWXjR8+3qpQ+hcDeFfbzWVPkz6b1CVyR2Ve+VZyEAAIIIIAAAggggEBvBMp+fPamkTQEAQQQkMCqih8pyr734mXf1bprK0jtF6hya6n37f7tb8pgaujnMcaft3jcn1USAggggAACCCCAAAK9EPDzVOKT3Xi8Fw2kEQgggEBBYGtNV7391N+J1yh4CUMBsUWT26gu8f9dufFXt6jOVGWFFfwsxty++hxACCCAAAIIIIAAAgj0SSB34tunNtIWBBBAoCjgFzDkvv9S8x/R+mcrNi1mxPRcBDZTqWXPKg378Amtx8sz5rKLRhbqNwuH/VQcjtyYFRBAAAEEEEAAAQQQ6IpA8WQ3TD/WlQZQTwQQQGAKgfdp2/C9V3X4lLY5UcFbMaeAn3BTX+Hm/5+q7Ksva70XTlgOm81e4E0qIrcf95x98ZSAAAIIIIAAAggggEAzArmTXs8nIYAAAkMQ8LPcrlaUfR/mlt2u7U5W8Dw4Icwo+bbEdyly+yA1/w0zqgvZ1iuQ2ndhXr0lkRsCCCCAAAIIIIAAAnMSuEPlhpPc4nBOVaJYBBBAYC4Cq6nUryqK34VVpx/Xtuco3q1YT0GaTmATbf5ZRVV/r/ewYh0FqRsCV6qauf27pBtNoJYIIIAAAggggAACCIwWyJ30Xjt6U9ZAAAEEeifg2xP3UeS+G8eZf5HyOUpBZ5AQKiS/0fIYRZXntxX3g/cZqVsCa6m6xf0YT3erNdQWAQQQQAABBBBAAIGMQHySWxzPbMJsBBBAYBACvurqSwq/XKH4/Tjp9IPKy7e2+mUNBynerlik8AsDnFZ+djCYfzdXS32V4E8UTyrGdT1N26yiIHVT4Ieqdm6fv7ObTaLWCCCAAAIIIDCpwAsm3ZDtEGi5gG+P+q1MHTnuMzDMRgCBwQn47ZgHK96h+F3FiopZpmeUuTuj3DHhDsDvKO5SvELxNcWLFT9X3KJYXXG54iWKWxV+AYSv3PPyVRVO/q73ei9SuCPRL4d4VPF7Cm+3XLGuwp1fCxRe7jz9vLTvKR5UvFnhsm3xGoU7EP9Y8YDiaYVv9XS43ssU1yhcnuvj8rdQvFzx54pXKdZUTJJ+qY0+pThQ4XJJ3RXYUFX3MZ5KPn5/X+HPAgkBBBBAAAEEEEAAgU4L5P7a3OlGUXkEEEBgRgJ+HtxHFHcqct+fzK/f5gJ5+wo5Ur8Eyj5HJ1doqq8S9a3GfkbjiYpXK0gIIIAAAggggAACCLRKIPcD0VcxkBBAAAEE8gJra9ExivsUue9S5k9u81O5+lZUUn8FtlbTcp8RXzlZljbVwicUxe0vLtuIZQgggAACCCCAAAIINC1QPGGNp5uuC+UhgAACXRXwlTe7Kj6hiL9HGR/P43b5+colP3yfNAyBW9XM3OdkSQnBPSXbHVqyHYsQQAABBBBAAAEEEGhUYKlKy53wNloRCkMAAQR6JOCOuN0Uvl3ubkXue5b5zz6P7gQZ+ZlfpOEJHKQm5z4HD2c4ti/Zxnn5GXEkBBBAAAEEEOiQAA+Z79DOoqoTCfgkNZX8QO8/Sy1gHgIIIIDAxAJ+QYHTFgq/CMEvHfCLAjZW3KvYSvETxUqK1yn8HKw1FesrVlTMKvnFC34ZwkOK7yt8K+21Cr9o4fWK8xRPKnx74JWK8Dwtv6RhXYXb9VJF2ZtG/RgDX9X2ecUZipsVzyhIwxbwCz58vKeSj6/7Cwveq+n/WZgXT/o4zuUXr8c4AggggAACCLREgI63luwIqjEzgVzHm+f7RyEJAQQQQKA9Au7YWqZwZ5c7JTz9R4rfVvgWTb/11FfcuePB3+PuUPPbQB03KXxec4XiRQquDBICae4CR6sG/5Cpxbc1353RcdpPE76tuyxx/l6mwzIEEEAAAQQQQACBRgX8l2H/OEtFoxWhMAQQQAABBBAYnIA7j1PnIGFe8eo1d9SFZamhX7pAQgABBBBAAAEEEECgVQKpE1fPIyGAAAIIIIAAArMWOEcF5M5FDisU/oOSdZ3HtwrrM4kAAggggAACCCCAwNwFcie7o27lmHvFqQACCCCAAAIIdF5gsVqQOxfxrdVx8nMBc+t6/rvjlRlHAAEEEEAAAQQQQKANAmUnsW2oH3VAAAEEEEAAgX4LlHWmbf5c0/0cw7L1vIyEAAIIIIAAAggggEDrBPy2utyJbOsqS4UQQAABBBBAoHcCJ6tFuXOR+55r7W0l63hbnu/2HBQDBBBAAAEEEEAAgfYJ5E52929fVakRAggggAACCPRMYA21J3cu4vlHjljudT6tICGAAAIIIIAAAggg0EqBspPdVlaYSiGAAAIIIIBArwTuVGvKzkdGLfMbUkkIIIAAAggggAACCLRSgOe8tXK3UCkEEEAAAQQGI/AmtXRU51pu+TWDUaKhCCCAAAIIIIAAAp0UWKJa505m+QtyJ3cplUYAAQQQQKBzArlzkVHzOVfp3K6mwggggAACCCCAwPAEcie1PxweBS1GAAEEEEAAgTkIHKsyc+cjufnehoQAAggggAACCCCAQOsFcie0y1pfcyqIAAIIIIAAAn0RyJ2PpOb7TackBBBAAAEEEEAAAQQ6IXCSapk6qfU8EgIIIIAAAggg0ITARiokdz4Sz39M6y1ookKUgQACCCCAAAIIIIBAXQLxCW08Xlf+5IMAAggggAACCIwS2EQrLFfE5yLx+KVa9sJRmbAcAQQQQAABBBBAAIG2CcQntfH4LW2rKPVBAAEEEEAAgd4L7KcWXqd4QuGOuK8q1lWQEEAAAQQQQAABBBDopMBVqnXc4RaPd7JBVBoBBBBAAAEEEEAAAQQQQAABBNopwCXs7dwv1Gp2Al+cXdbkjAACCCCAAAIIIIAAAggggAACCCCAwLAF4qvc4vFjh81C6xFAAAEEEEAAAQQQQAABBBBAoE6BF9SZGXkh0BGBZ1TP3LGfm9+RplFNBBBAAAEEEEAAAQQQQAABBBBoiwC3mrZlT1CPJgWebrIwykIAAQQQQAABBBBAAAEEEEAAgWEK0PE2zP0+9Fb/JANwZ2Y+sxFAAAEEEEAAAQQQQAABBBBAAIGxBbitbmwyNuiBgJ/rlkt8JnIyzEcAAQQQQAABBBBAAAEEEEAAgbEEuOJtLC5W7oFAWafbsh60jyYggAACCCCAAAIIIIAAAggggAACCCDQmMAilfSUIn6DaWq8sQpREAIIIIAAAggggAACCCCAAAIIIIAAAl0XWKIGpDrZivP273pDqT8CCCCAAAIIIIAAAggggAACCLRLgOdZtWt/UJv6BdzBViXxWaiixDoIIIAAAggggAACCCCAAAIIIFBZgGe8VaZixR4L7NzjttE0BBBAAAEEEEAAAQQQQAABBBCYk8CKcyqXYhFoiwBXurVlT1APBBBAAAEEEEAAAQQQQAABBHomwBVvPduhNOc3BJ75jTm/nvHmX48yhgACCCCAAAIIIIAAAggggAACCNQrwNU+9XqSWzsF3PkWH+t+7hudzu3cV9QKAQQQQAABBBBAAAEEEEAAAQQQQKCDAry5tIM7jSojgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAAAIIIIAAAggggAACCCCAQDsF/j/PauEk48bwYwAAAABJRU5ErkJggg=='
            },
            json: true
        };
    }
}