import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { CreateListPipe } from './pipes/create-list.pipe';
import { GetCommentPipe } from './pipes/get-comment.pipe';
import { GetFileUrlPipe } from './pipes/get-file-url.pipe';
import { GetFractionPipe } from './pipes/get-fraction.pipe';
import { RemoveTriggerCommentPipe } from './pipes/remove-trigger-comment.pipe';
import { RowHeaderNamePipe } from './pipes/row-header-name.pipe';
import { SliceObjectPipe } from './pipes/slice-object.pipe';
import { StatusNamePipe } from './pipes/status-name.pipe';
import { CommentsListPipe } from './pipes/comments-list';
import { IsImagePipe } from './pipes/is-image.pipe';
import { NoSignatureMapper } from './pipes/no-signature-mapper';

import { FirebaseService } from './services/firebase.service';
import { DataService } from './services/data.service';

@NgModule({
  declarations: [
    AppComponent,
    CreateListPipe,
    GetCommentPipe,
    GetFileUrlPipe,
    GetFractionPipe,
    RemoveTriggerCommentPipe,
    RowHeaderNamePipe,
    SliceObjectPipe,
    StatusNamePipe,
    CommentsListPipe,
    IsImagePipe,
    NoSignatureMapper
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'my-app-id'
    }),
    BrowserTransferStateModule,
    HttpModule
  ],
  providers: [
    FirebaseService,
    DataService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
