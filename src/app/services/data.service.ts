import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';

@Injectable()
export class DataService {
    constructor(private firebase: FirebaseService) { }

    public getImage(name: string) {
        // Create a root reference
        const storageRef = firebase.storage().ref();
        return name ? storageRef.child(name).getDownloadURL() : Promise.resolve('');
    }
}
