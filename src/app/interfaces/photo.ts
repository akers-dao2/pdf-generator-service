export interface IPhoto {
    name: string;
    downloadLink: string | Promise<string>;
    index?: number;
    uuid?: number;
    id?: number;
    fileName?: string;
}