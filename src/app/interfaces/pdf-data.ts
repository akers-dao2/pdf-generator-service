import { IClient, IPhoto, IReport, IUser } from './index';

export interface IPDFData {
    client: IClient;
    photos: IPhoto[];
    report: IReport;
    user: IUser;
    commentsAndRecommendations: any[];
    isCertification: boolean;
    dangerReportComments: any[];
    signature: string;
    comments: any[];
    recommendations: any[];
}
