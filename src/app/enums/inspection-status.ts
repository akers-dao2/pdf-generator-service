export enum InspectionStatus {
    failed,
    passed,
    'Not Applicable',
    'N/A - Weather',
    'N/A - Client refusal',
    'N/A - Inaccessible'
}