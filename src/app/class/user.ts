export class User {

    public key?: string;
    public jobListView?: string;

    constructor(
        public firstName?: string,
        public lastName?: string,
        public emailAddress?: string,
        public password?: string,
        public createJob = false,
        public editJob = false,
        public deleteJob = false,
        public printJob = false,
        public adminstration = false,
        public crew?: string,
        public CSIA = '',
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.password = password;
        this.createJob = createJob;
        this.editJob = editJob;
        this.deleteJob = deleteJob;
        this.printJob = printJob;
        this.adminstration = adminstration;
        this.crew = crew;
        this.CSIA = CSIA;
    }

    /**
     * Verifies all data is popluated
     *
     * @readonly
     *
     * @memberof User
     */
    public get isValid() {
        return Object.keys(this)
            .filter(key => !this.fieldsSkipValidate.includes(key))
            .every(key =>
                this[key] !== '' && this[key] !== undefined
            );
    }

    /**
     * A list of fields on the Client class to skip validate
     *
     * @readonly
     * @private
     *
     * @memberof User
     */
    private get fieldsSkipValidate() {
        return ['isValid', 'CSIA'];
    }
}
