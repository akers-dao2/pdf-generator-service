import { Component, Inject, OnInit } from '@angular/core';

import { IClient, IUser, IPhoto, IReport, IPDFData, IComment } from './interfaces/index';

import { InspectionStatus } from './enums/inspection-status';

import { Client } from './class/client';
import { User } from './class/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
  body, html, div, span{
    font-size: 8px;
    font-family: 'Muli', sans-serif;
    -webkit-print-color-adjust: exact;
  }
  #iframeContainer {
    height: 100%;
    width: 100%;
  }
  .logo {
      height: 0%;
      width: 50%;
  }
  hr {
      border-color: rgba(158, 158, 158, 0.17);
      margin: 0 7px;
      border-width: 1px;
      height: inherit;
  }

  ion-card img:not(.logo) {
      width: initial;
  }

  .border{
      border-style: solid !important;
  }

  .border-left,.border-right,.border-bottom,.border-top {
      border-color: #cecece !important;
  }

  .header{
      background-color: #073F80;
      color: #FFFFFF;
      border-color: #073F80;
  }

  .bl{
      border-left: solid 1px #cecece;
      padding-left: .5rem;
  }

  .width-170{
      width: 170px;
  }

  .pl{
      padding-left: .5rem;
  }

  .picName{
      color: whitesmoke;
      text-transform: uppercase;
      background-color:black;
  }

  .break{
    page-break-after: always;
  }

  .break-before{
    page-break-before: always;
  }

  .image{
    width:133px;
    height:200px;
  }

  .image-description{
    width:133px;
  }

  .signature-pad {
    font-size: 10px;
    width: 100%;
    height: 150px;
    border: 1px solid #e8e8e8;
    background-color: #fff;
  }

  .signature-pad--body
    canvas {
      width: 100%;
      height: 150px;
    }

  .no-signature{
    font-size: 11px;
    color: rgba(191, 188, 184, 0.64);
  }
  `]
})
export class AppComponent implements OnInit {
  public client: IClient = new Client();
  public user: IUser = new User();
  public requiredPics: IPhoto[] = [];
  public commentsAndRecommendations: any[] = [];
  public inspectItemsInReport: IReport[] = [];
  public isCertification: boolean;
  public signature = '';
  public dangerReportComments = [];
  public partialOrUnlinedStoves = [];
  public today: Date;
  public completionDate: number;

  constructor(
    @Inject('PDFData') public pdfData: IPDFData
  ) { }

  ngOnInit() {

    Object.keys(this.pdfData)
      .forEach(key => this[key] = this.pdfData[key]);

    this.dangerReportComments = this.getDangerReportComments(this.commentsAndRecommendations);
    this.partialOrUnlinedStoves = this.getPartialOrUnlinedStoves(this.inspectItemsInReport);


    this.today = this.completionDate ? new Date(this.completionDate) : new Date();
  }

  /**
   * Checks to see if there are any properties with a fraction to display
   *
   * @param {any} key
   * @returns
   * @memberof ReportComponent
   */
  public hasFraction(key) {
    return this.keysWithFractions.includes(key);
  }

  /**
   * Checks to see if there are any properties on the object to display
   *
   * @param {any} key
   * @returns
   * @memberof ReportComponent
   */
  public doesObjectHaveItems(key) {
    return this.keysWithPropsToDisplay.includes(key);
  }

  /**
   * The keys for properties that have an object as value
   *
   * @readonly
   * @private
   * @memberof ReportComponent
   */
  private get keysWithPropsToDisplay() {
    return [
      'crown',
      'stovePipe',
      'stoveClearances',
      'hearthPadProtection',
      'firebox',
      'hearth',
      'surroundClearance',
      'stackPipe'
    ];
  }

  /**
   * Keys that have fraction associated with them
   *
   * @readonly
   * @private
   * @memberof ReportComponent
   */
  private get keysWithFractions() {
    return [
      'outside1',
      'outside3',
      'inside1',
      'inside3',
    ];
  }

  private getDangerReportComments(inspectItemsInReport) {
    return this.inspectItemsInReport.filter(item => {

      if (!!Object.keys(item).length) {
        const keysWithComments = Object.keys(item).filter(key => item[key].hasOwnProperty('comments'));
        if (!!keysWithComments.length) {

          if (item.applianceType === 0) {
            return keysWithComments.some(key => {
              return item[key].status === InspectionStatus['failed'] && key === 'condition';
            });
          }

          return keysWithComments.some(key => {
            if (item[key].comments === null) {
              return false;
            }

            return item[key].comments.some((commentIndex) => {
              return this.pdfData['comments'][key][commentIndex].toLowerCase().includes('trigger');

            }) && item[key].status === InspectionStatus['failed'];
          });

        }
        return false;
      }

      return false;
    })
      .map(i => {
        return Object.assign({}, i, { name: this.newInspectName(i) });
      });

  }

  private newInspectName(item) {
    const chimneysList = this.chimneysList;
    const parentIndex = item.uuid[0];
    const chimneyName = this.getChimneyName(chimneysList, parentIndex);
    const applianceName = item.name;
    let name: string;

    if (chimneyName === applianceName) {
      name = chimneyName;
    } else {
      name = `${chimneyName}: ${applianceName}`;
    }

    return name;
  }

  private getChimneyName(chimneysList: IReport[], index: string) {
    return chimneysList.find(chimney => chimney.uuid === index).name;
  }

  private get chimneysList() {
    return this.inspectItemsInReport.filter(item => item.uuid.length === 1);
  }

  private getPartialOrUnlinedStoves(inspectItemsInReport: IReport[]) {
    return inspectItemsInReport
      .filter(i => i.applianceType === 3 && i.type === 'Insert' && (['Partial Liner', 'No Liner'].includes(i.ifInsert)))
      .reduce((applianceNames, item) => {
        const applianceName = this.getApplianceName(applianceNames, item, inspectItemsInReport);
        return [...applianceNames, applianceName];
      }, []);
  }

  private getApplianceName(applianceNames: string[], item, inspectItemsInReport: IReport[]) {
    const chimneyNumber = parseInt(item.uuid[0], 10) + 1;
    const applianceName = `Chimney ${chimneyNumber}: ${this.getFlueName(inspectItemsInReport, item)}: ${item.name}`;
    return applianceName;
  }

  private getFlueName(inspectItemsInReport: IReport[], inspectItem) {
    const itemIndex = inspectItemsInReport.findIndex(item => item.uuid === inspectItem.uuid);
    let flueName;
    for (let index = itemIndex; index > 0; index--) {
      if (inspectItemsInReport[index].applianceType === 0) {
        flueName = inspectItemsInReport[index].name;
        break;
      }
    }
    return flueName;
  }
}
