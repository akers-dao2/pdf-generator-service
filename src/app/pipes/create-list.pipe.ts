import { Pipe, PipeTransform } from '@angular/core';
import { IReport } from '../interfaces/report';
import { ApplianceTypes } from '../enums/appliances';

@Pipe({
  name: 'createList'
})
export class CreateListPipe implements PipeTransform {

  transform(inspectItems: IReport): any {
    const size = inspectItems.applianceType === 0 ? 2 : 3;

    let keysForInspectItems = Object.keys(inspectItems)
      .reduce((keys, key) => {
        const hasMatch = this.keysToRemove.some(keyToRemove => keyToRemove === key);
        return hasMatch ? keys : Array.of(...keys, key);
      }, []);

    const keySortOrder = this.keySortOrder(inspectItems.applianceType);

    if (keySortOrder) {
      keysForInspectItems = keySortOrder
        .reduce((keys, key) => {
          const keyToAdd = keysForInspectItems.find(key$ => key$ === key);
          return keyToAdd ? Array.of(...keys, keyToAdd) : keys;
        }, []);
    }

    const splitArray = [];

    while (keysForInspectItems.length > 0) {
      splitArray.push(keysForInspectItems.splice(0, size));
    }

    const splitArrayLength = splitArray.length - 1;
    const lastRecord = splitArray[splitArrayLength];

    if (lastRecord.length < splitArray[0].length) {
      while (splitArray[splitArrayLength].length < splitArray[0].length) {
        splitArray[splitArrayLength].push('n/a');
      }

    }

    if (this.isChimney(inspectItems) && (inspectItems).constructionType === 'No Chimney') {
      return [['constructionType']];
    }

    if (this.isFlue(inspectItems) && (inspectItems).material === 'No Flue Vent') {
      return [['material']];
    }

    return splitArray;
  }

  private isChimney(inspectItems) {
    return inspectItems.hasOwnProperty('constructionType');
  }

  private isFlue(inspectItems) {
    return inspectItems.hasOwnProperty('material');
  }

  private get keysToRemove() {
    return [
      'applianceType',
      'uuid',
      'id',
      'requiredPics',
      'name',
      'outside2',
      'outside4',
      'inside2',
      'inside4',
    ];
  }

  private keySortOrder(applianceType: number) {
    let order;

    switch (applianceType) {
      case ApplianceTypes.Chimney:
        order = this.chimneyOrder;
        break;
      case ApplianceTypes.Stoves:
        order = this.stoveOrder;
        break;
      case ApplianceTypes['Flue/Vent']:
        order = this.flueOrder;
        break;
      case ApplianceTypes['Masonry Fireplace']:
        order = this.firePlaceOrder;
        break;
      case ApplianceTypes['Prefab Fireplace']:
        order = this.firePlaceOrder;
        break;
      case ApplianceTypes['Heating Appliances']:
        order = this.heatingApplianceOrder;
        break;
      case ApplianceTypes['Dryer Vent']:
        order = this.dryerVentOrder;
        break;
    }

    return order;
  }

  private get chimneyOrder() {
    return [
      'constructionType',
      'roofMaterial',
      'flues',
      'crown',
      'brickStone',
      'mortar',
      'stucco',
      'flashing',
      'height',
      'structureIntegrity',
      'locationOnHouse',
      'location'
    ];
  }

  private get stoveOrder() {
    return [
      'type',
      'location',
      'fuel',
      'ifInsert',
      'firebrick',
      'thimble',
      'stoveCondition',
      'stovePipe',
      'stoveClearances',
      'hearthPadProtection',
    ];
  }

  private get flueOrder() {
    return [
      'material',
      'length',
      'condition',
      'insulation',
      'outside1',
      'outside3',
      'inside1',
      'inside3',
      'cap',
    ];
  }

  private get firePlaceOrder() {
    return [
      'location',
      'fuelType',
      'fireboxMaterial',
      'firebox',
      'hearth',
      'surroundClearance',
      'damper',
      'smokeChamber',
    ];
  }

  private get dryerVentOrder() {
    return [
      'dryerMake',
      'distanceToTermination',
      'flowBeforeCleaning',
      'flowAfterCleaning',
      'numberOfTurns',
      'fuel',
      'dryerType',
      'ductMaterial',
      'terminationLocation',
      'ventCondition',
      'terminationCondition',
      'accessibility',
      'other',
    ];
  }

  private get heatingApplianceOrder() {
    return [
      'location',
      'type',
      'details',
      'unit',
      'btu_input',
      'gph_input',
      'stackPipe',
    ];
  }
}
