import { Pipe, PipeTransform, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Pipe({
    name: 'noSignatureMapper'
})
export class NoSignatureMapper implements PipeTransform {

    constructor(

    ) { }

    transform(prop: string): any {
        return this.mapper[prop];
    }

    private get mapper() {
        return {
            'customerRefused': 'Customer Refused',
            'notAvailable': 'Not Available'
        };
    }


}
