import { Pipe, PipeTransform, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { IPDFData } from '../interfaces/index';

@Pipe({
  name: 'getComment'
})
export class GetCommentPipe implements PipeTransform {

  constructor(
    @Inject('PDFData') public pdfData: IPDFData
  ) { }

  transform(index: number, key: string, type: string): any {
    return this.comment(key, type, index);
  }

  private comment(key, type, index) {
    return this.pdfData[type][key][index];
  }

}
