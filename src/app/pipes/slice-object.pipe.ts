import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sliceObject'
})
export class SliceObjectPipe implements PipeTransform {

  transform(value: {}, prop?: string): any {

    const orderKeys = !this.sortOrder(prop).length ? Object.keys(value) : this.sortOrder(prop);

    const keyList = orderKeys
      .reduce((keys, key) => {
        const hasMatch = this.keysToRemove.some(keyToRemove => keyToRemove === key);
        return hasMatch ? keys : Array.of(...keys, key);
      }, []);

    return Array.of('name', ...keyList);
  }

  private sortOrder(prop) {
    let sortOrder: any[];

    switch (prop) {
      case 'crown':
        sortOrder = this.crown;
        break;
      case 'firebox':
        sortOrder = this.firebox;
        break;
      default:
        sortOrder = [];
        break;
    }

    return sortOrder;
  }

  private get keysToRemove() {
    return [
      'status',
      'pictures',
      'comments',
      'recommendations',
      'additionalComments',
      'enableXSide',
      'enableYSide',
      'enableZSide'
    ];
  }

  private get crown() {
    return [
      'status',
      'material',
      'length',
      'width'
    ];
  }

  private get firebox() {
    return [
      'status',
      'height_whole',
      'width_whole',
      'depth_whole',
      'square_foot_total',
    ];
  }

}
