import { Pipe, PipeTransform } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { IReport } from '../interfaces/report';
import { InspectionStatus } from '../enums/inspection-status';

@Pipe({
  name: 'statusName'
})
export class StatusNamePipe implements PipeTransform {
  constructor() {

  }
  transform(inspectItems: any): any {
    const titleCase = new TitleCasePipe();
    return inspectItems.status !== undefined ? titleCase.transform(this.modifiedPassedOrFailed(inspectItems)) : inspectItems;
  }

  private modifiedPassedOrFailed(inspectItems) {
   return this.mapper(InspectionStatus[inspectItems.status]);
  }

  private mapper(status) {
    const m = {
      passed: 'No Defect Detected',
      failed: 'Defective',
    };
    return m[status] || status;
  }

}
