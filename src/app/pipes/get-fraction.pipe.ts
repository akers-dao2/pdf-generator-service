import { Pipe, PipeTransform } from '@angular/core';
import { IReport } from '../interfaces/report';

import * as Fraction from 'fraction.js';

@Pipe({
  name: 'getFraction'
})
export class GetFractionPipe implements PipeTransform {

  transform(inspectItems: IReport, key?: string): any {
    const keyNumber = parseInt(key.slice(-1), 10);
    const keyString = key.slice(0, -1);
    const fractionKey = keyNumber + 1;
    const newKey = keyString + fractionKey;
    const fraction = new Fraction(inspectItems[newKey]).toFraction(true);
    const [numerator, denominator] = fraction.split('/');
    return denominator !== undefined ? `<sup>${numerator}</sup>&frasl;<sub>${denominator}</sub>` : '';
  }

}
