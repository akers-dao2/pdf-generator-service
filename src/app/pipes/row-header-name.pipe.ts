import { Pipe, PipeTransform } from '@angular/core';
import { ApplianceTypes } from '../enums/appliances';

@Pipe({
  name: 'rowHeaderName'
})
export class RowHeaderNamePipe implements PipeTransform {

  transform(value: any, chimneyType: boolean): any {
    return this.nameMapper(value, chimneyType) || value;
  }

  private nameMapper(name, chimneyType) {
    const type = chimneyType ? 'Crown' : 'Flue';
    const headers = {
      constructionType: 'Construction Type',
      roofMaterial: 'Roof Material',
      flues: 'Number of Flues',
      locationOnHouse: 'Location On House',
      crown: 'Crown',
      brickStone: 'Brick Stone',
      mortar: 'Mortar',
      stucco: 'Stucco',
      flashing: 'Flashing',
      height: 'Height',
      structureIntegrity: 'Structure Integrity',
      material: `${type} Material`,
      condition: 'Condition',
      insulation: 'Insulation',
      cap: 'Cap',
      length: `${type} Length`,
      inside1: 'Flue Size Inside',
      inside3: 'Flue Size Inside',
      outside1: 'Flue Size Outside',
      outside3: 'Flue Size Outside',
      location: 'Location',
      flueType: 'Flue Type',
      fuelType: 'Fuel Type',
      fireboxMaterial: 'Firebox Material',
      firebox: 'Firebox',
      hearth: 'Hearth',
      surroundClearance: 'Surround Clearance',
      damper: 'Damper',
      smokeChamber: 'Smoke Chamber',
      dryerMake: 'Dryer Make',
      distanceToTermination: 'Distance To Termination',
      flowBeforeCleaning: 'Flow Before Cleaning',
      flowAfterCleaning: 'Flow After Cleaning',
      numberOfTurns: 'Number Of Turns',
      fuel: 'Fuel',
      dryerType: 'Dryer Type',
      ductMaterial: 'Duct Material',
      terminationLocation: 'Termination Location',
      terminationCondition: 'Termination Condition',
      accessibility: 'Accessibility',
      other: 'Other',
      type: 'Type',
      ifInsert: 'If Insert',
      stoveCondition: 'Stove Condition',
      stovePipe: 'Stove Pipe',
      hearthPadProtection: 'Hearth Pad Protection',
      firebrick: 'Firebrick',
      thimble: 'Thimble',
      stoveClearances: 'Stove Clearances',
      btu_input: 'BTU Input',
      gph_input: 'GPH Input',
      details: 'Details',
      unit: 'Unit',
      stackPipe: 'Stack Pipe',
      pipe_size: 'Pipe Size',
      pipe_type: 'Pipe Type',
      to_wall: 'To Wall',
      to_ceiling: 'To Ceiling',
      width_whole: 'Width',
      height_whole: 'Height',
      depth_whole: 'Depth',
      square_foot_total: 'Square Inch Total',
      depth: 'Depth',
      right: 'Right',
      left: 'Left',
      x: 'X',
      y: 'Y',
      z: 'Z',
      sideZ: 'Side Z',
      back: 'Back',
      front: 'Front',
      width: `${type} Width`,
      ventCondition: 'Vent Condition'
    };

    return headers[name];
  }

}
