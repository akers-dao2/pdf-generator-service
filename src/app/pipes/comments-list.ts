import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IPDFData, IReport } from '../interfaces/index';
import { GetCommentPipe } from './get-comment.pipe';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMapTo';
import 'rxjs/add/operator/reduce';

@Pipe({
    name: 'commentsList'
})
export class CommentsListPipe implements PipeTransform {

    constructor(
        @Inject('PDFData') public pdfData: IPDFData
    ) { }

    transform(inspectItems: IReport, isDanger = false): any {

        return Observable.from(Object.keys(inspectItems))
            .mergeMapTo(Observable.of(this.pdfData.comments), (key, comments) => ({ key, comments }))
            .reduce<any>((keys, { key, comments }) => {
                const hasMatch = Object.keys(inspectItems[key])
                    .some(key$ => {
                        const hasKey = this.keysToDisplay.includes(key$);

                        if (hasKey) {
                            if (inspectItems[key].comments === null || !inspectItems[key].comments.length) {
                                return false;
                            }

                            return inspectItems[key].comments.some(i => {
                                if (key === 'condition') {
                                    return true;
                                }

                                return isDanger ? comments[key][i].includes('Trigger') : true;
                            });
                        }

                        return false;
                    });

                return hasMatch ? Array.of(...keys, key) : keys;
            }, []);

    }

    private get keysToDisplay() {
        return [
            'comments',
            'recommendations'
        ];
    }
}
