import { Pipe, PipeTransform } from '@angular/core';
import { DataService } from '../services/data.service';

@Pipe({
  name: 'getFileUrl'
})
export class GetFileUrlPipe implements PipeTransform {
  constructor(
    private dataService: DataService
  ) { }

  transform(downloadLink: any, args?: any): any {
    const fileName = new RegExp(/(?:o\/)(.+)(?=\?)/).exec(downloadLink as string)[1];

    return this.dataService.getImage(fileName) as any;

  }

}
