import { Pipe, PipeTransform, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Pipe({
  name: 'isImage'
})
export class IsImagePipe implements PipeTransform {

  constructor(

  ) { }

  transform(image: string): any {
    return /data:image\/png/.test(image);
  }


}
