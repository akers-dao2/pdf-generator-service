import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IPDFData } from '../interfaces/index';
import { Observable } from 'rxjs/Observable';

@Pipe({
  name: 'removeTriggerComment'
})
export class RemoveTriggerCommentPipe implements PipeTransform {
  constructor(
    @Inject('PDFData') public pdfData: IPDFData
  ) { }

  transform(comments: number[], key?: any): any {
    return  comments.filter(commentIndex => !this.pdfData['comments'][key][commentIndex].toLowerCase().includes('trigger'));
  }

}
