## PDF Generator Service

---------
	
Service for generating a pdf from html

**Installation:**

You need to node and npm. At the root of the project type:

```node
npm install
```

**Update Firebase Configuration:**
```
src/app/services/firebase.service.ts
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. Angular (https://angular.io)
1. Angular Server-side Rendering (SSR) (https://angular.io/guide/universal)
1. FireBase (https://www.firebase.com)
1. Express (https://expressjs.com/)
